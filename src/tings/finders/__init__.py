# -*- coding: utf-8 -*-
import asyncio
import logging
from abc import ABCMeta, abstractmethod
from typing import Type, Dict, List, Any, Tuple, Union

from observable import Observable

from frtls.lists import ensure_sequence
from plugtings.core import EntryPointIndex, Plugting
from tings.defaults import TINGS_INPUT_KEY
from tings.ting import TingSeed, TingCallbackInput
from tings.tings import Tings

log = logging.getLogger("tings")


def get_tings_id(tings_obj_or_id: Union[str, Tings]) -> str:

    if isinstance(tings_obj_or_id, Tings) or issubclass(
        tings_obj_or_id.__class__, Tings
    ):
        return tings_obj_or_id.id
    else:
        return tings_obj_or_id


class TingFindersPlugting(Plugting):
    def __init__(self):

        index = EntryPointIndex(entry_point="tings.finders")
        super(TingFindersPlugting, self).__init__(index)

    def get_base_class(self) -> Type:
        return TingFinder

    def get_default_init_data(self, alias) -> Dict:

        return None

    def get_default_plugin(self) -> str:

        return "plugin-finder"


class TingFinder(metaclass=ABCMeta):
    def __init__(self):

        self._tings_map = {}
        self._seed_map = {}
        self._observable = Observable()

    @property
    def registered_tings_ids(self) -> List[str]:

        return list(self._tings_map.keys())

    def register_tings(
        self, tings: Tings, find_data: Any, skip_initial_find=False
    ) -> List[TingSeed]:

        if tings.id in self._tings_map.keys():
            raise Exception(f"Already registered seeds for tings id '{tings.id}'.")

        input_names = tings.spec.ting_input_names

        extra_seed_values = {}
        if TINGS_INPUT_KEY in input_names:
            extra_seed_values[TINGS_INPUT_KEY] = TingCallbackInput(tings)

        self._tings_map[tings.id] = {"tings": tings, "seeds": {}}
        self._register_tings(tings.id, find_data, input_names, extra_seed_values)

        if not skip_initial_find:
            self.find(tings.id)

    def find(self, *tings_id) -> List[TingSeed]:

        if len(tings_id) != 1:
            raise NotImplementedError("Multiple tings not supported yet.")

        tings_id = tings_id[0]

        tings_id = get_tings_id(tings_id)

        self._get_seeds(tings_id)

        # for s in seeds:
        #     if isinstance(s, tuple):
        #         seed = s[0]
        #         cached = s[1]
        #     else:
        #         seed = s
        #         cached = None
        #
        #     self.seed_added(seed, tings_id=tings_id, cached_values=cached)
        #
        # return seeds

    def seed_added(self, seed: TingSeed, tings_id, cached_values=None):

        if seed.id in self._seed_map.keys():
            raise ValueError(f"Seed with id '{seed.id}' already exists.")

        if tings_id not in self._tings_map.keys():
            raise ValueError(f"No tings object with id '{tings_id}'.")

        self._tings_map[tings_id]["seeds"][seed.id] = seed
        self._seed_map[seed.id] = {"seed": seed, "tings_id": tings_id}
        self._tings_map[tings_id]["tings"].add_ting(seed, cached_values=cached_values)
        self._observable.trigger(tings_id, event_type="seed_added", seed=seed)
        self._observable.trigger(
            "seeds", tings_id=tings_id, event_type="seed_added", seed=seed
        )

    def seed_changed(self, seed: TingSeed):

        if seed.id not in self._seed_map.keys():
            raise ValueError(
                f"Can't update seed, seed with id '{seed.id}' does not exist."
            )
        self._seed_map[seed.id]["seed"] = seed
        tings_id = self._seed_map[seed.id]["tings_id"]
        self._tings_map[tings_id]["seeds"][seed.id] = seed
        self._tings_map[tings_id]["tings"].update_ting(seed)
        self._observable.trigger(tings_id, event_type="seed_changed", seed=seed)
        self._observable.trigger(
            "seeds", tings_id=tings_id, event_type="seed_changed", seed=seed
        )

    def seed_removed(self, seed_id: str):

        seed_data = self._seed_map.pop(seed_id)
        seed = seed_data["seed"]
        seed.ting = None
        tings_id = seed_data["tings_id"]
        self._tings_map[tings_id]["tings"].remove_ting(seed.id)
        self._observable.trigger(tings_id, event_type="seed_removed", seed=seed)
        self._observable.trigger(
            "seeds", tings_id=tings_id, event_type="seed_removed", seed=seed
        )

    def find_seed(self, id) -> Tuple[TingSeed, Tings]:

        seed_data = self._seed_map[id]
        return (seed_data["seed"], seed_data["tings"])

    def supports_watch(self):

        return hasattr(self, "create_watch_tasks")

    def collect_watch_tasks(self):

        if not self.supports_watch():
            raise Exception(
                f"{type(self)} does not support watching for changed tings/seeds."
            )
        tasks = {}
        for tings_data in self._tings_map.values():
            tings = tings_data["tings"]
            self._observable.on("seed_added", tings.add_ting)
            self._observable.on("seed_removed", tings.remove_ting)
            self._observable.on("seed_changed", tings.update_ting)
            t = self.create_watch_tasks(tings.id)
            t = ensure_sequence(t)
            tasks.setdefault(tings, []).extend(t)
        # obs.off("seed_added", tings._add_ting)
        # obs.off("seed_removed", tings._remove_ting)
        # obs.off("seed_changed", tings._update_ting)
        return tasks

    def watch(self, *event_handlers):

        loop = asyncio.get_event_loop()
        watch_tasks = self.collect_watch_tasks()

        for tings, tasks in watch_tasks.items():
            for ev in event_handlers:
                tings.add_observer("item_change", ev)
            for task in tasks:
                func = task["func"]
                args = task.get("args")
                loop.create_task(func(*args))

        loop.run_forever()

    @abstractmethod
    def _get_seeds(self, tings_id: str) -> List["TingSeed"]:
        pass

    @abstractmethod
    def _register_tings(
        self,
        tings_id: str,
        find_data: Any,
        input_names: List[str],
        extra_seed_values: Dict[str, Any] = None,
    ):

        pass

    # async def _watch(self, tings_id):
    #
    #     pass
