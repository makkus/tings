# -*- coding: utf-8 -*-
import copy
import logging
from abc import ABCMeta, abstractmethod
from typing import Dict, Type, List, Any

from frtls.classes import generate_valid_identifier
from frtls.lists import FrklList, ensure_sequence
from plugtings.core import EntryPointIndex, Plugting
from tings.defaults import TINGS_DEFAULT_TING_FINDERS_FOLDER, TINGS_DEFAULT_SPEC_FOLDER
from tings.finders import TingFinder
from tings.spec import TingSpec
from tings.ting import TingSeed

log = logging.getLogger("tings")


class TingFinderPluginPlugting(Plugting):
    def __init__(self):

        index = EntryPointIndex(entry_point="tings.finders.plugins")
        super(TingFinderPluginPlugting, self).__init__(index)

    def get_base_class(self) -> Type:
        return TingFinderPlugin

    def get_default_init_data(self, alias: str) -> Dict:

        return None


class TingFinderPlugin(metaclass=ABCMeta):
    def __init__(self, finder: "PluggableTingFinder", tings_id: str, config):

        self._finder = finder
        self._tings_id = tings_id
        self._config = config

    @abstractmethod
    def find_items(self) -> Dict[str, List[Dict]]:

        pass

    @abstractmethod
    def provides(self) -> List[str]:

        pass


class PluggableTingSeedBuilder(object):
    def __init__(
        self,
        finder: "PluggableTingFinder",
        tings_id: str,
        plugin_type: str,
        input_names: List[str],
        extra_seed_values: Dict[str, Any],
        inputs_map: Dict[str, str],
        plugin: TingFinderPlugin,
        seed_spec: Dict,
    ):

        self._finder = finder
        self._tings_id = tings_id
        self._plugin_type = plugin_type
        self._input_names = input_names
        self._extra_seed_values = extra_seed_values
        self._inputs_map = inputs_map
        self._plugin = plugin
        self._plugin_provides = plugin.provides()
        self._cls_name = generate_valid_identifier(
            f"{self._tings_id.capitalize()}{self._plugin_type.capitalize()}Seed"
        )

        if seed_spec is None:
            seed_spec = {
                "properties": [],
                "extra_input": self._plugin_provides,
                "ting_class_name": self._cls_name,
            }

        seed_spec = TingSpec.from_dict(seed_spec)

        self._seed_spec = seed_spec
        self._cls = self._create_seed_seed_cls()

    def seed_added(self, seed_id, seed):

        ts = self.create_ting_seed(seed_id, seed)

        self._finder.seed_added(seed=ts, tings_id=self._tings_id)

    def seed_updated(self, seed_id, seed):

        ts = self.create_ting_seed(seed_id, seed)
        self._finder.seed_changed(seed=ts)

    def seed_removed(self, seed_id):

        self._finder.seed_removed(seed_id=seed_id)

    def _create_seed_seed_cls(self) -> Type:

        cls = self._seed_spec.build_class()
        return cls

    @property
    def seed_cls(self) -> Type:
        return self._cls

    def find_ting_seeds(self) -> Dict[str, "PluginTingSeed"]:

        self._plugin.find_items(builder=self)

    def create_ting_seed(self, id, seed_data):

        s = self.seed_cls()
        for k, v in seed_data.items():
            setattr(s, k, v)
        seed = PluginTingSeed(
            id=id,
            seed_seed=s,
            input_names=self._input_names,
            extra_seed_values=self._extra_seed_values,
            inputs_map=self._inputs_map,
        )

        return seed

    def create_watch_tasks(self):

        tasks = self._plugin.watch_tasks(self)
        return tasks


class PluginTingSeed(TingSeed):
    def __init__(
        self,
        seed_seed,
        input_names,
        extra_seed_values: Dict[str, Any],
        inputs_map: Dict[str, str],
        id: str = None,
    ):

        self._seed_seed = seed_seed
        self._input_names = input_names
        super(PluginTingSeed, self).__init__(
            id=id, extra_seed_values=extra_seed_values, inputs_map=inputs_map
        )
        self._apply_seed_seed()

    def _apply_seed_seed(self):

        to_set = set()
        for i_n in self._input_names:

            if i_n not in self._inputs_map.keys():
                continue

            v = self._inputs_map[i_n]
            if v in self._extra_seed_values.keys():
                continue

            value = getattr(self._seed_seed, v)
            self._seed_values[v] = value
            to_set.add(i_n)

        self._set_ting_input(*list(to_set))

    def set_seed_input_value(self, **inputs):

        raise Exception("Method not used in this subclass of TingSeed.")


class PluggableTingFinder(TingFinder):
    """The TingFinder to end all Tingfinders! A generic base class that can re-use `TingProperty` classes."""

    def __init__(self):

        super(PluggableTingFinder, self).__init__()
        self._plugting = TingFinderPluginPlugting()

        self._builders: Dict[str, List[PluggableTingSeedBuilder]] = {}

    def _register_tings(self, tings_id, find_data, input_names, extra_seed_values):

        plugin_list = ensure_sequence(find_data["plugins"])
        plugins_config_list = FrklList(*plugin_list, key="plugin")
        inputs_map = find_data.get("inputs_map", None)

        if inputs_map is None:
            inputs_map = {}
        else:
            inputs_map = copy.copy(inputs_map)

        for i_n in input_names:
            if i_n not in inputs_map.keys():
                inputs_map[i_n] = i_n

        builders = []
        for plugin_data in plugins_config_list:

            plugin_type = plugin_data["plugin"]
            plugin_config = plugin_data["config"]

            plugin: TingFinderPlugin = self._plugting.create_obj(
                plugin_type,
                init_data={
                    "finder": self,
                    "tings_id": tings_id,
                    "config": plugin_config,
                },
            )
            # plugin_provides = plugin.provides()

            # check that all inputs are covered
            # import pp
            # pp(inputs_map)
            # pp(plugin_provides)
            # for k, v in inputs_map.items():
            #
            #     if v not in plugin_provides:
            #         if v == k:
            #             msg = ""
            #         else:
            #             msg = f" (for resolving input '{k}')"
            #         raise Exception(
            #             f"Unusable Ting finder: does not provide attribute '{v}'{msg}."
            #         )
            builder = PluggableTingSeedBuilder(
                finder=self,
                tings_id=tings_id,
                plugin_type=plugin_type,
                input_names=input_names,
                extra_seed_values=extra_seed_values,
                inputs_map=inputs_map,
                plugin=plugin,
                seed_spec=plugin_data.get("seed_spec", None),
            )
            builders.append(builder)

        self._builders[tings_id] = builders

    def _get_seeds(self, tings_id):

        for builder in self._builders[tings_id]:

            builder.find_ting_seeds()

    def supports_watch(self):

        for tings_id, builders in self._builders.items():
            for builder in builders:

                if not hasattr(builder._plugin, "watch_tasks"):
                    return False

        return True

    def create_watch_tasks(self, tings_id):

        tasks = []
        for builder in self._builders[tings_id]:

            t = builder.create_watch_tasks()
            tasks.extend(t)

        return tasks


DEFAULT_TING_FINDER_FIND_DATA = {
    "_type": "plugin-finder",
    "plugins": [
        {
            "files": {
                "config": {
                    "base_paths": [TINGS_DEFAULT_TING_FINDERS_FOLDER],
                    "matchers": [{"path-regex": {"regex": "\\.ting_finder"}}],
                },
                "seed_spec": {
                    "properties": [
                        {
                            "path-parts": {
                                "path_property": "_seed_path",
                                "target_property": "_file_name_without_ext",
                                "include_dir_name": False,
                                "include_extension": False,
                            }
                        },
                        {
                            "text-content": {
                                "path_property": "_seed_path",
                                "target_property": "_file_content",
                            }
                        },
                        {
                            "dict-content": {
                                "source_property": "_file_content",
                                "target_property": "_dict_content",
                                "content_string_format": "yaml",
                            }
                        },
                    ]
                },
            }
        }
    ],
    "inputs_map": {"_id": "_file_name_without_ext"},
}

DEFAULT_TING_SPEC_FIND_DATA = {
    "_type": "plugin-finder",
    "plugins": [
        {
            "files": {
                "config": {
                    "base_paths": [TINGS_DEFAULT_SPEC_FOLDER],
                    "matchers": [{"path-regex": {"regex": "\\.spec"}}],
                },
                "seed_spec": {
                    "properties": [
                        {
                            "path-parts": {
                                "path_property": "_seed_path",
                                "target_property": "_file_name_without_ext",
                                "include_dir_name": False,
                                "include_extension": False,
                            }
                        },
                        {
                            "text-content": {
                                "path_property": "_seed_path",
                                "target_property": "_file_content",
                            }
                        },
                        {
                            "dict-content": {
                                "source_property": "_file_content",
                                "target_property": "_dict_content",
                                "content_string_format": "yaml",
                            }
                        },
                    ]
                },
            }
        }
    ],
    "inputs_map": {"_id": "_file_name_without_ext"},
}
