# -*- coding: utf-8 -*-
import copy
import logging
import uuid
from abc import ABCMeta
from functools import partial
from typing import Dict, Any, Callable, Union, Mapping, List

from observable import Observable

from frtls.classes import generate_valid_identifier
from frtls.lists import ensure_sequence
from plugtings.core import ensure_object
from tings.addons import TingAddon
from tings.defaults import TING_INPUT_NOT_SET
from tings.spec import TingSpec

log = logging.getLogger("tings")


class MultiResult(object):
    """Marker object that let's a `TingProperty` return multiple values, even though only one is requested.

    This is useful in some situations so certain computations don't need to be repeated and can be cached.
    """

    def __init__(self, **results):
        self._results = results

    @property
    def results(self):
        return self._results

    def __repr__(self):

        return f"[MultiResult: {self._results}]"


class TingPropertyFunc(object):
    def __init__(
        self, property_name: str, requirements: Dict[str, Any], func: Callable
    ):

        self._property_name = property_name
        self._requirements = requirements
        self._func = func
        self._result = None

    @property
    def property_name(self) -> str:
        return self._property_name

    @property
    def requirements(self) -> Dict[str, Any]:
        return self._requirements

    @property
    def func(self) -> Callable:
        return self._func

    @property
    def result(self) -> Any:
        return self._result

    @result.setter
    def result(self, result):
        self._result = result

    def execute(self):

        self._result = self.func(
            requirements=self.requirements, property_name=self.property_name
        )
        return self._result


class TingCallbackInput:
    def __init__(self, source):

        self._access_map = {}
        self._source = source
        self._source.add_observer("item_change", self._handle_invalidate)

        self._observable = Observable()

    def _handle_invalidate(self, change_type, item, source, item_paths=None, **kwargs):

        # print("INVALIDATE")
        # print(change_type)
        # print(item.name)
        # print(source.id)
        # print(item_paths)
        # print(self._access_map)
        if not item_paths:
            return

        for item_path in item_paths:
            # print("ITEMPATH: {}".format(item_path))
            # print("ACCESS MAP")
            # for k, v in self._access_map.items():
            #     print(f"   {k}: {v}")
            # print("X-----")
            invalidate = self._access_map.get(item_path, None)
            if invalidate is None:
                continue
            for i in invalidate:
                obj = i["obj"]
                path = i["path"]
                log.debug("invalidating: {} - {}".format(obj, path))
                obj._ting_invalidate_(path)
            self._access_map.pop(item_path)

    def get_item_value(self, item_path, accessor_obj, accessor_path=None):

        value = self._source.get_value(item_path)

        if hasattr(accessor_obj, "_ting_invalidate_"):
            self._access_map.setdefault(item_path, []).append(
                {"obj": accessor_obj, "path": accessor_path}
            )
        else:
            print("NO INV")

        # print("GET: {} - {}".format(item_path, value))

        # print("ACCESS MAP")
        # if hasattr(accessor_obj, "name"):
        #     print("NAME: {}".format(accessor_obj.name))
        # pp(self._access_map.keys())
        return value

    def add_event_callback(self, ting, input_name):
        def callback(*args, **kwargs):

            ting._ting_invalidate_(input_name)

        return callback


class TingMeta(type):
    """Metaclass to construct `Ting` classes from `TingSpec`s.

    The purpose of this metaclass is to avoid having to implement common code in every implementation of a `Ting` class or subclass.
    This Metaclass will create read/write properties for inputs, and read-only (Python) properties for Ting property methods.

    It does that by analyzing the provides/requires outputs for all `TingProperty`s in the `TingSpec`. Any missing
    dependencies will be assumed to be inputs. The dependent properties are wired in a way  that when any of the upstream
    inputs are changed, their values will be invalidated (depending on the actual implementation of the `Ting` class of
    course -- it's possible to just ignore those events).

    """

    registered_ting_classes = {}

    # def __call__(cls, *args, **kwargs):
    #
    #     print("__CALL__")
    #     self = type.__call__(cls, *args)
    #
    #
    #     print("__CALL_FINISHED__")
    #     return self

    def __new__(
        cls, clsname, superclasses, attributes, spec: Union[TingSpec, Mapping] = None
    ):

        if spec is None:
            ting_class = super(TingMeta, cls).__new__(
                cls, clsname, superclasses, attributes
            )
            return ting_class

        if spec is None:
            raise Exception("'spec' argument to class constructor required.")

        spec: TingSpec = ensure_object(TingSpec, spec)

        # -------------------------------------------------------------------------
        # input and property attributes
        inputs = TingMeta.create_input_attributes(spec)
        for i_name, input in inputs.items():

            if i_name in attributes.keys():
                raise Exception(
                    f"Can't add input '{i_name}': attribute with that name already exists."
                )
            attributes[i_name] = input

        properties = TingMeta.create_property_attributes(spec)
        for p_name, prop in properties.items():

            if p_name in attributes.keys():
                raise Exception(
                    f"Can't add property '{p_name}': attribute with that name already exists."
                )
            attributes[p_name] = prop

        def ting_property_dict(self):

            result = {}
            for pn in self._ting_property_names_:
                result[pn] = getattr(self, pn)
            return result

        attributes["_ting_property_dict_"] = property(fget=ting_property_dict)

        def ting_input_dict(self):

            result = {}
            for i_n in self._ting_input_names_:
                result[i_n] = getattr(self, i_n)
            return result

        attributes["_ting_input_dict_"] = property(fget=ting_input_dict)

        def ting_value_dict(self):

            result = {}
            result.update(self._ting_input_dict_)
            result.update(self._ting_property_dict_)
            return result

        attributes["_ting_value_dict_"] = property(fget=ting_value_dict)

        # -------------------------------------------------------------------------
        # addon attribute

        attributes["_ting_addon_list_"] = spec.ting_addons

        # # access registry
        # # observable stuff
        # def access_registry():
        #     def getter(selfobj):
        #         obs = getattr(selfobj, "_ting_access_registry_obj_")
        #         if obs is None:
        #             setattr(selfobj, "_ting_access_registry_obj_", TingAccessRegistry(selfobj))
        #         obs = getattr(selfobj, "_ting_access_registry_obj_")
        #         return obs
        #
        #     return property(fget=getter)
        #
        # attributes["_ting_access_registry_"] = access_registry()
        # attributes["_ting_access_registry_obj_"] = None

        # -------------------------------------------------------------------------
        # observable stuff
        def observable():
            def getter(selfobj):
                obs = getattr(selfobj, "_ting_observable_obj_")
                if obs is None:
                    setattr(selfobj, "_ting_observable_obj_", Observable())
                obs = getattr(selfobj, "_ting_observable_obj_")
                return obs

            return property(fget=getter)

        attributes["_ting_observable_"] = observable()
        attributes["_ting_observable_obj_"] = None

        def on_event():
            def on(selfobj, event: str, *handlers: Callable):
                selfobj._ting_observable_.on(event, *handlers)

            return on

        attributes["_ting_on_"] = on_event()

        def trigger_event():
            def trigger(selfobj, event: str, *args: Any, **kwargs: Any):
                selfobj._ting_observable_.trigger(event, *args, **kwargs)

            return trigger

        attributes["_ting_trigger_"] = trigger_event()

        # -------------------------------------------------------------------------
        # invalidation
        def wrap_create_access_method():
            def get_value(selfobj, property_name, accessor):

                if property_name not in selfobj._ting_property_names_:
                    raise Exception(
                        f"Can't use '_ting_get_property_value_' method with a non-property Ting attribute: {property_name}"
                    )

                value = getattr(selfobj, property_name)
                selfobj._ting_access_registry_.register_access(
                    accessor=accessor, property_name=property_name, value=value
                )
                return value

            return get_value

        attributes["_ting_get_property_value_"] = wrap_create_access_method

        def ting_invalidate(self, *attributes):

            a = set()
            i = set()
            for attr in attributes:
                # self._ting_trigger_(
                #     input,
                #     event_type="invalidated",
                #     attribute=input,
                #     attribute_type="input",
                # )
                # for dep in self._ting_input_dependencies_[input]:
                #     self._ting_trigger_(
                #         dep,
                #         event_type="invalidated",
                #         attribute=dep,
                #         attribute_type="property",
                #     )

                if attr in self._ting_input_names_:
                    deps = self._ting_input_dependencies_[attr]
                    a.update(deps)
                    i.add(attr)
                elif attr in self._ting_property_names_:
                    a.add(attr)
                    deps = self._ting_property_dependencies_[attr]
                    a.update(deps)

            self._ting_trigger_("invalidate", properties=list(a), inputs=list(i))

        attributes["_ting_invalidate_"] = ting_invalidate

        # -----------------------------------------------------------------------------------------
        # misc
        def ting_info(selfobj):

            result = {}
            result["_ting_input_names_"] = selfobj._ting_input_names_
            result["_ting_property_names"] = selfobj._ting_property_names_
            result["_ting_input_dict_"] = selfobj._ting_input_dict_
            result["_ting_property_dict_"] = selfobj._ting_property_dict_
            return result

        attributes["_ting_info_"] = property(fget=ting_info)

        ting_class = super(TingMeta, cls).__new__(
            cls, clsname, superclasses, attributes
        )
        cls.registered_ting_classes[clsname] = ting_class

        return ting_class

    def create_input_attributes(spec):

        input_names = list(spec.ting_inputs.keys())

        def wrap_create_input_property(input):
            def getter(selfobj):
                return getattr(selfobj, f"_ting_input_{input}_")

            def setter(selfobj, value):

                # old_value = getattr(selfobj, f"_ting_input_{input}_")
                # TODO: maybe we can optimize here if old_value == value? needs to make sure things like
                # file changes where the path stays the same work still though
                setattr(selfobj, f"_ting_input_{input}_", value)
                selfobj._ting_invalidate_(input)

            prop = property(fget=getter, fset=setter)
            # prop = ObservableProperty(event=a, observable="_ting_inputs_observer_", fget=getter, fset=setter)
            return prop

        inputs = {"_ting_input_names_": input_names}
        for input in input_names:
            inputs[input] = wrap_create_input_property(input)

        result = {}
        for i_name, i_attr in inputs.items():

            if i_name in result.keys():
                raise Exception(f"Can't add input attribute: {i_name} already exists.")
            result[i_name] = i_attr

        def get_input_dict(self):

            result = {}
            for input in self._ting_input_names_:
                result[input] = getattr(self, input)
            return result

        result["_ting_input_dict_"] = get_input_dict
        result["_ting_input_dependencies_"] = spec.ting_inputs

        def ting_set_inputs(selfobj, **inputs):

            for input, value in inputs.items():
                # old_value = getattr(selfobj, f"_ting_input_{input}_")
                setattr(selfobj, f"_ting_input_{input}_", value)

            selfobj._ting_invalidate_(*inputs.keys())

        result["_ting_set_input_values_"] = ting_set_inputs

        return result

    def create_property_attributes(spec: TingSpec):

        properties = {}

        for prop in spec.ting_properties:

            md = prop._metadata_check()
            prop_func = getattr(prop, "get_value")

            requires = prop._requires_check()
            provides = prop._provides_check()

            attrs = {}

            def create_wrapper(func):
                def func_wrap(requirements: Dict[str, Any], property_name):

                    result = func(requirements, property_name)
                    return result

                return func_wrap

            for prov in provides:

                if prov in attrs.keys():
                    raise Exception(
                        f"Can't assembley Ting class: duplicate property attribute '{prov}'"
                    )

                attrs[prov] = {
                    "func": partial(create_wrapper(func=prop_func), property_name=prov),
                    "metadata": md,
                    "requirements": requires,
                }

            for attr_name, attr in attrs.items():
                if attr_name in properties.keys():
                    raise Exception(
                        f"Can't add property: '{attr_name}' already exists."
                    )

                properties[attr_name] = attr

        def get_property_wrap(self, property_name):

            result = self._ting_retrieve_property_values_([property_name])
            return result[property_name]

        property_names = list(properties.keys())
        result = {
            "_ting_properties_": properties,
            "_ting_property_names_": property_names,
            "_ting_get_property_": get_property_wrap,
            "_ting_property_dependencies_": spec.ting_property_dependencies,
        }

        for pn in property_names:
            result[pn] = property(fget=partial(get_property_wrap, property_name=pn))

        return result


class Ting(metaclass=TingMeta):
    """The default, vanilla implementation of a `Ting`.

    This class very basic implementations of methods that are required by some logic implemented by this Metaclass.
    It will process the `TingProperty` `get_value` method every time the value is requested, and as such is
    not very useful. Either use any of the provided implementations of a `Ting` class, or use this class as the base
    for your own implementation. The main reason for writing your own implementation is to control when and where the
    computation for the property attributes of your `Ting` happens.

    To do this, overwrite one or several of this classes' methods (and look at the other provided sub-classes for
    inspiration).
    """

    @classmethod
    def create_class(cls, spec, class_name=None):

        if class_name is None:
            class_name = generate_valid_identifier(first_character_uppercase=True)

        return type(class_name, (cls,), {}, spec=spec)

    def __new__(cls, *args, **kwargs):

        self = super(Ting, cls).__new__(cls)

        for arg in cls._ting_input_names_:
            setattr(self, f"_ting_input_{arg}_", TING_INPUT_NOT_SET)

        for addon in cls._ting_addon_list_:
            cls = addon["cls"]
            init_data = addon["init_data"]
            obj: TingAddon = cls(ting=self, **init_data)

            for prov in obj._provides_check():
                if hasattr(self, prov):
                    raise Exception(
                        f"Can't add TingAddon '{cls}', Ting object already has attribute '{prov}'."
                    )
                method = getattr(obj, prov)
                setattr(self, prov, method)
        return self

    def _ting_execute_(self, funcs: List[TingPropertyFunc]):
        """Execute a list of `get_value` methods from`TingProperty` classes.

        The most likely method to overwrite. By default, this executes every computation to get a properties value
    sequential. Check the `CachedTing` class for how to add a cache to your class, for example. Or you could start a new
    thread for every property. Or use a remote executor to do the work...

           Args:
               - *funcs*: the (wrapped) functions to execute
        """

        for func in funcs:
            func.execute()

    def _ting_available_property_values_(self) -> Dict[str, Any]:
        """Return all properties where the values are already present.

        This is mostly to optimize scheduling, and indicates to the (default) scheduling algorithm that the value for this property is already present, or requires
        only a very short computation, and returns a dictionary with all of those properties and their values.
        """
        return {}

    def _ting_set_properties_(self, prop_dict: Dict[str, Any]):
        """Set available properties in case the implementation of the `Ting` supports it.

        This is useful to pre-warm caches etc.
        """
        pass

    def _ting_get_requirement_keys_(
        self, property_names, only_direct_requirements=False
    ) -> Union[List[str], List[List[str]]]:
        """Return a list of lists of properties that need processing before being able to resolve the requested properties."""

        property_names = ensure_sequence(property_names, set_ok=True)

        all_req_names = []
        for pn in property_names:
            for r in self._ting_properties_[pn]["requirements"]:
                if r not in all_req_names:
                    all_req_names.append(r)

        missing = []
        if not all_req_names:
            # no requirements at all
            return None

        for r in all_req_names:
            # check whether we can satisfy all requirments via inputs
            if r not in self._ting_input_names_:
                missing.append(r)

        if not missing:
            return None

        cached = self._ting_available_property_values_()
        still_missing = []
        # try to see whether all of the properties might be cached already
        for m in missing:

            if m not in cached.keys():
                still_missing.append(m)

        if not still_missing:
            return None

        if only_direct_requirements:
            return still_missing

        result = [still_missing]
        while True:
            req_reqs = self._ting_get_requirement_keys_(
                still_missing, only_direct_requirements=True
            )
            if req_reqs:
                still_missing = req_reqs
                result.insert(0, req_reqs)
            else:
                break

        return result

    def _ting_get_requirement_values_for_property_(
        self, property_name, retrieved_values=None, force=False
    ):

        reqs = self._ting_properties_[property_name]["requirements"]

        # print("GET REQUIREMENT VALUES: {} - {}".format(property_name, retrieved_values))

        values = {}
        if not reqs:
            return values

        if retrieved_values is not None:
            temp = []
            for r in reqs:
                if r in retrieved_values.keys():
                    values[r] = retrieved_values[r]
                else:
                    temp.append(r)

            if not temp:
                return values

            reqs = temp

        missing = []
        for r in reqs:
            if r in self._ting_input_names_:
                values[r] = getattr(self, r)
            else:
                missing.append(r)

        if not missing:
            return values

        cached = self._ting_available_property_values_()
        still_missing = []
        for m in missing:

            if m in cached:
                values[m] = cached[m]
            else:
                still_missing.append(m)

        if not still_missing:
            return values

        if not force:
            raise Exception(
                f"Can't get all requirement values for '{property_name}', missing: {still_missing}"
            )

        raise NotImplementedError(
            "'forced' retrieval of requirement values not implemented yet."
        )

    def _ting_retrieve_property_values_(self, property_names):

        property_names = ensure_sequence(property_names, set_ok=True)

        reqs_reqs = self._ting_get_requirement_keys_(property_names)
        if not reqs_reqs:
            reqs_reqs = [property_names]
        else:
            reqs_reqs.append(property_names)

        requirements = {}
        last_result = None
        for reqs in reqs_reqs:
            funcs = []
            for req in reqs:
                v = self._ting_get_requirement_values_for_property_(
                    req, retrieved_values=requirements
                )

                requirements.update(v)

                prop_req = {}
                for r in self._ting_properties_[req]["requirements"]:
                    req_value = requirements[r]

                    if isinstance(req_value, TingCallbackInput):
                        # print("XXXXXXX")
                        # print(f"r: {r}")
                        # print(f"req: {req}")
                        part_func = partial(
                            req_value.get_item_value,
                            accessor_obj=self,
                            accessor_path=req,
                        )
                        prop_req[r] = part_func
                    else:
                        prop_req[r] = req_value

                def wrap(req2, prop_req2, props2):

                    prop_func = TingPropertyFunc(
                        property_name=req2,
                        requirements=prop_req2,
                        func=props2[req2]["func"],
                    )
                    return prop_func

                funcs.append(
                    wrap(req2=req, prop_req2=prop_req, props2=self._ting_properties_)
                )

            self._ting_execute_(funcs)
            last_result = {}
            for func in funcs:
                r = func.result
                if not isinstance(r, MultiResult):
                    last_result[func.property_name] = func.result
                    requirements[func.property_name] = func.result
                else:
                    for k, v in r.results.items():
                        last_result[k] = v
                        requirements[k] = v

        return last_result

    def __repr__(self):
        input_dict = self._ting_input_dict_
        return f"[{self.__class__.__name__}: input={input_dict}, properties={self._ting_property_names_}]"


class TingSeed(metaclass=ABCMeta):
    @classmethod
    def from_ting(cls, ting: Ting, id: str = None):

        obj = cls(id=id, ting=ting)
        return obj

    def __init__(
        self,
        id: str = None,
        ting: Ting = None,
        extra_seed_values: Dict[str, Any] = None,
        inputs_map: Dict[str, str] = None,
    ):

        if id is None:
            id = str(uuid.uuid4())
        self._id = id
        self._ting = None

        if extra_seed_values is None:
            extra_seed_values = {}
        self._extra_seed_values = extra_seed_values

        if inputs_map is None:
            inputs_map = {}
        else:
            inputs_map = inputs_map
        self._inputs_map = inputs_map

        self._seed_values = copy.copy(extra_seed_values)

        if ting is not None:
            self.ting = ting

    def __repr__(self):

        return f"[TingSeed: id={self.id}, values={self.get_values()}]"

    @property
    def id(self):
        """Return the seed id.

        The id is only relevant for the lengh of the session, and should not be persisted.
        """
        return self._id

    @property
    def ting(self) -> Ting:
        return self._ting

    @ting.setter
    def ting(self, ting: Ting):
        self._ting = ting
        if ting is None:
            return

        self._set_all()

    def _set_all(self):

        if self._ting is None:
            return

        self._set_ting_input(*self._ting._ting_input_names_)

    def _set_ting_input(self, *input_names: str):

        if self._ting is None:
            return

        # in case we have to 'translate' the seed property to the ting input property
        inputs = {}
        for input_name in input_names:
            translated_input_name = self._inputs_map.get(input_name, input_name)

            val = self._seed_values.get(translated_input_name, TING_INPUT_NOT_SET)

            if val == TING_INPUT_NOT_SET:
                continue

            inputs[input_name] = val

        self._ting._ting_set_input_values_(**inputs)

    def get_values(self, translated=False):
        """Return all current values of this seed.

        If a `Ting` is set, the value of `_ting_input_names` will be used as result key set. If not, all set values will
        be returned.

        Args:
            - *translated*: if True, reverse the dictionary and return all the input arguments as they will be set on the target `Ting` object
        """

        if not translated:
            if self._ting is not None:
                names = self._ting._ting_input_names_
            else:
                names = self._seed_values.keys()
            result = {}
            for i_n in names:
                result[i_n] = self._seed_values[i_n]
            return result
        else:
            if self._ting is None:
                return {}

            result = {}
            for i_n in self._ting._ting_input_names_:
                key = self._inputs_map.get(i_n, i_n)
                val = self._seed_values.get(key, TING_INPUT_NOT_SET)
                result[i_n] = val
            return result

    def set_seed_input_value(self, **inputs):

        for k, v in inputs.items():
            self._seed_values[k] = v

        if self._ting is None:
            return

        to_set = set()
        for key in inputs.keys():

            for i_n in self._ting._ting_input_names_:
                i_n = self._inputs_map.get(i_n, i_n)

                if i_n == key:
                    to_set.add(i_n)

        self._set_ting_input(*list(to_set))
