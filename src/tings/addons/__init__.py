# -*- coding: utf-8 -*-
from abc import ABCMeta, abstractmethod
from collections import Callable
from typing import Type

from frtls.lists import is_list_of_strings
from plugtings.core import Plugting, EntryPointIndex


class TingAddon(metaclass=ABCMeta):
    def __init__(self, ting):
        self._ting = ting
        self._check_attributes()

    def _check_attributes(self):

        for prov in self._provides_check():

            if not hasattr(self, prov):
                raise Exception(
                    f"Invalid TingAddon subclass, claims to provide attribute '{prov}' but does not."
                )

        for req in self._requires_check():

            if not hasattr(self._ting.__class__, req) and not hasattr(self._ting, req):
                raise Exception(
                    f"Invalid TingAddon subclass, requires attribute '{req}' from child Ting, but it's not available."
                )

    def get_requirement(self, key):

        return getattr(self._ting, key)

    def get_requirements(self):

        result = {}
        for attr in self._requires_check():
            a = getattr(self._ting, attr)
            if isinstance(a, Callable):
                a = a()
            result[attr] = a
        return result

    def _provides_check(self):

        prov = self.provides()
        if isinstance(prov, str):
            return [prov]
        elif is_list_of_strings(prov):
            return prov
        else:
            return ValueError(
                f"Invalid 'provides' value for TingAddon '{self.__class__.__name__}', must be string or list of strings.",
                prov,
            )

    @abstractmethod
    def provides(self):
        pass

    def _requires_check(self):

        req = self.requires()
        if isinstance(req, str):
            return [req]
        elif is_list_of_strings(req):
            return req
        else:
            return ValueError(
                f"Invalid 'requires' value for TingAddon '{self.__class__.__name__}', must be string or list of strings.",
                req,
            )

    def requires(self):

        return []


class TingAddonsPlugting(Plugting):
    def __init__(self):

        index = EntryPointIndex(entry_point="tings.addons")
        super(TingAddonsPlugting, self).__init__(index)

    def get_base_class(self) -> Type:

        return TingAddon

    def get_default_init_data(self, alias):

        return None
