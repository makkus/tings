properties:
  - id
  - file-size:
      path_property: _path
      target_property: file_size
  - file-name:
      path_property: _path
      target_property: file_name
