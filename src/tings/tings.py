# -*- coding: utf-8 -*-

"""Main module."""
import fnmatch
import logging
import uuid
from abc import ABCMeta, abstractmethod
from datetime import datetime
from typing import Sequence, Mapping, Dict, Callable, Union, Type, Any

from observable import Observable
from sortedcontainers import SortedDict

from frtls.lists import ensure_sequence
from plugtings.core import ensure_object
from plugtings.tings import PlugtingTings
from tings.defaults import (
    FROZEN_TING_SPEC_KEY,
    FROZEN_TING_LIST_KEY,
    FROZEN_TING_METADATA_KEY,
    NO_CACHE_KEY,
)
from tings.properties import TingProperty
from tings.spec import TingSpec
from tings.ting import Ting, TingSeed

log = logging.getLogger("tings")


class TingsRegistry(metaclass=ABCMeta):
    @abstractmethod
    def create_ting_finder(self, alias: str, config: Dict):
        pass

    @abstractmethod
    def create_property_type(self, alias: str, config: Dict) -> TingProperty:
        pass

    # @abstractmethod
    # def create_ting_builder(self, alias: str, config: Dict) -> TingBuilder:
    #     pass

    @abstractmethod
    def get_ting_spec(self, alias):
        pass

    @abstractmethod
    def get_tings(self, tings_id):
        pass


class TingsAutoRegistry(TingsRegistry):
    def __init__(self):

        self._plugting_tings = PlugtingTings()

        self._specs = {}
        self._tings = {}

    @property
    def finders(self) -> Dict[str, "TingFinder"]:
        return self._finder_plugting

    @property
    def specs(self) -> Dict[str, TingSpec]:
        return self._specs

    @property
    def property_types(self) -> Dict[str, Type[TingProperty]]:
        return self._property_types_plugting

    @property
    def tings(self) -> Dict[str, "Tings"]:
        return self._tings

    def create_ting_finder(self, alias: str = None, config=None):
        return self._plugting_tings.create_object_from_plugin(
            "tings.finders", entry_point=alias, init_data=config
        )

    def create_property_type(self, alias: str, config=None) -> TingProperty:
        return self._plugting_tings.create_object_from_plugin(
            "tings.properties", entry_point=alias, init_data=config
        )

    # def create_ting_builder(self, alias: str, config=None) -> TingBuilder:
    #     return self._plugting_tings.create_object_from_plugin("tings.builders", entry_point=alias, init_data=config)

    def add_spec(self, alias, spec_dict):

        if alias in self._specs.keys():
            raise Exception(f"Spec with alias '{alias}' already exists.")

        spec = TingSpec(**spec_dict)
        self._specs[alias] = {"spec_dict": spec_dict, "spec": spec}

    def get_ting_spec(self, alias):
        temp = self._specs.get(alias, None)
        if temp is None:
            return None
        return temp["spec"]

    def add_tings(self, tings_init_data):

        id = tings_init_data.get("id", None)
        if id is None:
            id = str(uuid.uuid4())
            tings_init_data["id"] = id

        if id in self._tings.keys():
            raise Exception(f"Tings object with id {id} already exists.")

        self._tings[id] = Tings.from_dict(tings_init_data)

    def get_tings(self, tings_id):
        temp = self._tings.get(tings_id, None)
        if temp is None:
            return None
        return temp["tings"]


class Tings(object):
    """A class to hold a set of *Ting* objects.
    """

    @classmethod
    def from_dict(cls, data: Dict[str, Any], registry: TingsRegistry = None) -> "Tings":

        if registry is None:
            registry = TingsAutoRegistry()

        if not isinstance(data, Mapping):
            raise TypeError(f"Invalid input type: {type(data)}")

        ting_spec_data = data.get("ting_spec", None)
        if ting_spec_data is None:
            raise ValueError("Can't create 'tings' object: no 'ting_spec' key.")
        ting_spec = TingSpec(**ting_spec_data)

        tings_default_index = data.get("default_index", None)

        id = data.get("id", None)

        return cls.create(ting_spec=ting_spec, id=id, default_index=tings_default_index)

    @classmethod
    def create(
        cls,
        ting_spec: Union[TingSpec, Mapping],
        id: str = None,
        default_index: str = None,
    ):

        ting_spec = ensure_object(TingSpec, ting_spec)

        return cls(ting_spec=ting_spec, id=id, default_index=default_index)

    @classmethod
    def unfreeze(
        cls, path, id: str = None, target_class_name: str = None, freezer="json"
    ) -> "Tings":

        freezer_obj = ensure_object(
            entry_point_or_base_class="tings.freezers", data=freezer
        )
        tings = freezer_obj.unfreeze(target_class_name=target_class_name, path=path)

        return tings

    @classmethod
    def create_from_archive(
        cls, archive: "TingsArchive", id: str = None, target_class_name: str = None
    ) -> "Tings":

        from tings.finders.archive import ArchiveTingFinder

        ting_finder = ArchiveTingFinder()

        tings = cls(ting_spec=archive.get_ting_spec())

        ting_finder.register_tings(tings, find_data=archive)
        ting_finder.find(tings)

        return tings

    def __init__(self, ting_spec: TingSpec, id: str = None, default_index: str = None):

        if id is None:
            id = str(uuid.uuid4())
        self._id = id

        self._spec: TingSpec = ting_spec
        self._ting_cls = self._spec.default_ting_class

        self._event_log_level = logging.INFO

        self._observable = Observable()
        self.add_observer("item_change", self._ting_event)

        self._tings: Dict[str, Ting] = {}

        self._indexes = {}
        self._default_index = default_index
        if default_index:
            self.add_index(default_index, is_default_index=True)

    def __eq__(self, other):

        if not isinstance(other, self.__class__):
            return False
        return self.id == other.id

    def __hash__(self):
        return hash(self.id)

    @property
    def id(self):
        return self._id

    @property
    def ting_class_name(self):
        return self.spec.ting_class_name

    def add_observer(self, event, callback: Callable):

        self._observable.on(event, callback)

    def add_ting(self, seed: TingSeed, cached_values: Dict[str, Any] = None):

        if seed.id in self._tings.keys():
            raise ValueError(f"Seed with id '{seed.id}' already added.")

        ting = self.ting_cls()
        seed.ting = ting

        if cached_values is not None:
            ting._ting_populate_cache(cached_values)
        self._tings[seed.id] = ting

        for index_name, index in self._indexes.items():
            index_property = index["index_property"]
            unique = index["unique_values"]
            tings = index["tings"]
            value = getattr(ting, index_property)
            if unique:
                if value in tings.keys():
                    raise Exception(
                        f"Can't add ting: index '{index_name}' already contains key '{value}'"
                    )
                tings[value] = ting
            else:
                values = tings.setdefault(value, [])
                values.append[ting]

        self._observable.trigger(
            event="item_change",
            change_type="add",
            item=ting,
            source=self,
            item_paths=self._get_item_paths(ting),
        )

        return ting

    def remove_ting(self, seed_id):

        if seed_id not in self._tings.keys():
            raise ValueError(
                f"Can't remove ting: no seed with id '{seed_id}' available."
            )

        ting = self._tings.pop(seed_id)
        for index in self._indexes.values():
            index_property = index["index_property"]
            unique = index["unique_values"]
            tings = index["tings"]
            value = getattr(ting, index_property)
            if unique:
                tings.pop(value)
            else:
                values = tings[value]
                values.remove[ting]

        self._observable.trigger(
            event="item_change",
            change_type="delete",
            item=ting,
            source=self,
            item_paths=self._get_item_paths(ting),
        )

    def update_ting(self, seed: TingSeed):

        if seed.id not in self._tings.keys():
            raise ValueError(
                f"Can't update ting: no seed with id '{seed.id}' available."
            )

        ting = self._tings[seed.id]
        seed.ting = ting
        self._observable.trigger(
            event="item_change",
            change_type="update",
            item=ting,
            source=self,
            item_paths=self._get_item_paths(ting),
        )

    def _ting_event(self, change_type, item, item_paths, source):

        log.log(self._event_log_level, msg=f"{change_type}: {item}")

    def add_index(
        self,
        index_property: str,
        index_name: str = None,
        is_default_index: bool = False,
        sorted=True,
        unique_values: bool = True,
    ):

        if index_name is None:
            index_name = index_property

        if index_property not in self.ting_cls._ting_property_names_:
            raise Exception("Can't add an index for a non-'ting' property.")

        if not self._indexes:
            is_default_index = True

        if index_name in self._indexes.keys():
            raise Exception(
                f"Can't add index '{index_name}': index already exists for tings with id '{self.id}'"
            )

        if is_default_index and not unique_values:
            raise Exception("Default index can't have non-unique values.")

        if sorted:
            tings_dict = SortedDict()
        else:
            tings_dict = {}
        self._indexes[index_name] = {
            "index_property": index_property,
            "tings": tings_dict,
            "unique_values": unique_values,
        }
        index = self._indexes[index_name]["tings"]

        for ting in self.tings:
            attr = getattr(ting, index_property)
            if not attr:
                raise Exception(
                    f"Can't create index: attribute '{index_property}' empty for at least one ting item."
                )
            if not isinstance(attr, str):
                raise Exception(
                    f"Can't create index: attribute '{index_property}' not a string (value: {attr})."
                )
            if unique_values:
                index[attr] = ting
            else:
                index.setdefault(attr, []).append(ting)

        if is_default_index:
            self._default_index = index_name

        return index

    def get_index(self, index_name: str = None) -> Dict[str, Ting]:

        if index_name is None:
            i = self._default_index
        else:
            i = index_name

        index = self._indexes.get(i, None)
        if index is None:
            if not index_name:
                raise Exception("No default index created yet.")
            else:
                raise Exception(f"No index with name '{index_name}'")
        else:
            return index["tings"]

    def get_ting(self, key, index_name=None):

        if index_name is None:
            index_name = self._default_index

        if index_name not in self._indexes.keys():
            raise Exception("No index with name: {}".format(index_name))

        value = self._indexes[index_name]["tings"].get(key, None)
        return value

    def get_value(self, item_path):

        if item_path == "tings":
            return self.tings

        tokens = item_path.split(".")
        index = tokens[0]

        if len(tokens) == 1:
            return self.get_index(tokens[0])

        key = tokens[1]
        ting = self.get_ting(key=key, index_name=index)

        if len(tokens) == 2:
            return ting

        property = tokens[2]
        return getattr(ting, property)

    def _get_item_paths(self, ting):

        index_change_paths = ["tings"]
        for index, metadata in self._indexes.items():
            property_name = metadata["index_property"]
            t = getattr(ting, property_name)
            index_change_paths.append(f"{index}.{t}")

        return index_change_paths

    def __repr__(self):

        return f"[{self.__class__.__name__}: tings={list(self._tings.keys())}]"

    @property
    def ting_cls(self):

        return self._ting_cls

        # if self._ting_cls is not None:
        #     return self._ting_cls
        #
        # self._ting_cls = self.ting_builder.assemble_class(
        #     ting_class_name=self._target_class_name, spec=self.spec
        # )
        # return self._ting_cls

    # @property
    # def ting_builder(self) -> TingBuilder:
    #     return self._ting_builder

    @property
    def spec(self) -> TingSpec:
        return self._spec

    @property
    def tings(self) -> Sequence[Ting]:

        return self._tings.values()

    def to_value_list(self):

        result = []
        for t in self.tings:

            props = t._ting_value_dict_
            result.append(props)
        return result

    def export(
        self, metadata: Dict = None, include_no_cache_properties: bool = False
    ) -> "TingsArchive":

        if metadata is None:
            metadata = {}

        additional_properties = ensure_sequence(
            metadata.get("additional_properties", [])
        )

        tings = []
        for t in self.tings:

            ting_data = {}
            ting_data["seed"] = t._ting_input_dict_

            prop_names = set()
            for p in t._ting_property_names_:
                for ap in additional_properties:
                    if fnmatch.fnmatch(p, ap):
                        prop_names.add(p)
                        break

            temp = set()
            if not include_no_cache_properties:
                for ap in prop_names:
                    no_cache = t._ting_properties_[ap]["metadata"].get(NO_CACHE_KEY, [])

                    if ap in no_cache:
                        continue

                    temp.add(ap)
                prop_names = temp

            properties = {}

            for ap in prop_names:
                properties[ap] = getattr(t, ap)

            if properties:
                ting_data["properties"] = properties

            tings.append(ting_data)

            metadata["archive_created"] = datetime.utcnow().isoformat()

        ting_spec_dict = self.spec.to_dict()

        return TingsArchive(
            ting_spec_dict=ting_spec_dict, ting_list=tings, metadata=metadata
        )

    def freeze(self, path, force=False, freezer="json", **metadata):

        json_freezer = ensure_object(
            entry_point_or_base_class="tings.freezers", data=freezer
        )
        json_freezer.freeze(tings=self, metadata=metadata, path=path, force=force)


class TingsArchive(object):
    @classmethod
    def from_dict(cls, data):

        spec = data[FROZEN_TING_SPEC_KEY]
        ting_list = data[FROZEN_TING_LIST_KEY]
        metadata = data[FROZEN_TING_METADATA_KEY]

        return TingsArchive(ting_spec_dict=spec, ting_list=ting_list, metadata=metadata)

    def __init__(self, ting_spec_dict, ting_list, metadata=None):

        from tings.tings import TingSpec

        if isinstance(ting_spec_dict, TingSpec):
            ting_spec_dict = ting_spec_dict.to_dict()
        self._ting_spec_dict = ting_spec_dict
        self._ting_list = ting_list
        if metadata is None:
            metadata = {}
        self._metadata = metadata

    @property
    def ting_spec_dict(self):
        return self._ting_spec_dict

    def get_ting_spec(self):
        from tings.tings import TingSpec

        return TingSpec.from_dict(self._ting_spec_dict)

    @property
    def ting_list(self):
        return self._ting_list

    @property
    def metadata(self):
        return self._metadata

    def __repr__(self):

        return f"[TingArchive: tings={self.ting_list}]"

    def to_dict(self):

        return {
            FROZEN_TING_SPEC_KEY: self._ting_spec_dict,
            FROZEN_TING_LIST_KEY: self._ting_list,
            FROZEN_TING_METADATA_KEY: self._metadata,
        }
