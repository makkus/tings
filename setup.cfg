# This file is used to configure your project.
# Read more about the various options under:
# http://setuptools.readthedocs.io/en/latest/setuptools.html#configuring-setup-using-setup-cfg-files

[metadata]
name = tings
description = Create Python objects from things.
author = Markus Binsteiner
author-email = markus@frkl.io
license = "The Parity Public License 6.0.0"
url = https://gitlab.com/frkl/tings
long-description = file: README.md
long-description-content-type = text/markdown
# Change if running only on Windows, Mac or Linux (comma-separated)
platforms = any
# Add here all kinds of additional classifiers as defined under
# https://pypi.python.org/pypi?%3Aaction=list_classifiers
classifiers =
    Development Status :: 3 - Alpha
    Programming Language :: Python

[options]
zip_safe = False
packages = find:
include_package_data = True
package_dir =
    =src

setup_requires =
    setuptools_scm
    setuptools_scm_git_archive

install_requires =
    appdirs>=1.4.3
    watchgod>=0.5
    observable>=1.0.3
    sortedcontainers>=2.1.0
    effect>=1.1.0
    plugtings>=0.1.dev10
    frtls[all]


python_requires = >=3.6

[options.packages.find]
where = src
exclude =
    tests


[options.extras_require]
cli =
    urwid>=2.1.0

testing =
    pytest
    pytest-cov
    tox

develop =
    flake8
    ipython
    black
    pip-tools
    pre-commit
    watchdog
    wheel
    pipdeptree
    isort
    setuptools_scm
    cruft
    pp-ez
    nuitka

docs =
    portray
    livereload
    formic2

[options.entry_points]
console_scripts =
    tings = tings.interfaces.cli:cli

plugtings =
    tings.properties = tings.properties:TingPropertiesPlugting
    tings.finders = tings.finders:TingFindersPlugting
    tings.freezers = tings.freezers:TingsFreezerPlugting

tings.properties =
    dict-value = tings.properties.core:DictValue
    id = tings.properties.core:Id
    mirror = tings.properties.core:Mirror
    static-dict = tings.properties.core:StaticDict
    file-name = tings.properties.files:FileName
    file-size = tings.properties.files:FileSize
    text-content = tings.properties.files:TextContent
    dict-content = tings.properties.core:DictContent
    ting-finder-obj = tings.properties.internal:TingFinderObj
    ting-spec-obj = tings.properties.internal:TingSpecObj
    path-parts = tings.properties.files:PathParts
    template-input-schema = tings.properties.templating:TemplateInputSchema
    parent-type = tings.properties.types:ParentType
    type-chain = tings.properties.types:TypeChain
    type-dict = tings.properties.types:TypeDict
    input-type = tings.properties.core:InputType
    input-content = tings.properties.core:InputContent
    dict-merge = tings.properties.core:DictMerge

tings.addons :
    to-dict = tings.addons.core:ToDict
    create-tings = tings.addons.internal:CreateTings
    create-find-data = tings.addons.internal:CreateFindData
    watch-tings = tings.addons.internal:WatchTings

tings.finders =
;   files-finder = tings.finders.files:FilesTingFinder
    archive-finder = tings.finders.archive:ArchiveTingFinder
;   dictionaries-finder = tings.finders.dictionaries:DictionariesTingFinder
    plugin-finder = tings.finders.pluggable:PluggableTingFinder

tings.finders.plugins =
    files = tings.finders.pluggable.files:FileTingFinderPlugin

tings.utils.files.matchers =
    path-regex = tings.utils.files:PathRegexMatcher

tings.freezers =
    json = tings.freezers.json:JsonFreezer

[test]
# py.test options when running `python setup.py test`
# addopts = --verbose
extras = True

[tool:pytest]
# Options for py.test:
# Specify command line options as you would do when invoking py.test directly.
# e.g. --cov-report html (or xml) for html/xml output or --junitxml junit.xml
# in order to write a coverage file that can be read by Jenkins.
addopts =
    --cov tings --cov-report term-missing
    --verbose
norecursedirs =
    dist
    build
    .tox
testpaths = tests
pep8maxlinelength = 88


[aliases]
build = bdist_wheel
release = build upload

[bdist_wheel]
# Use this option if your package is pure-python
universal = 1

[build_sphinx]
source_dir = docs
build_dir = docs/_build

[devpi:upload]
# Options for the devpi: PyPI server and packaging tool
# VCS export must be deactivated since we are using setuptools-scm
no-vcs = 1
formats = sdist, bdist_wheel

[flake8]
# Some sane defaults for the code style checker flake8
exclude =
    .tox
    build
    dist
    .eggs
    docs/conf.py
    .git
    __pycache__
ignore = F405, W503, E501
max-line-length = 88
