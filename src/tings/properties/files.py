# -*- coding: utf-8 -*-
import os
from typing import List, Dict, Any

from tings.properties import TingProperty


class FileSize(TingProperty):
    def __init__(self, path_property, target_property):

        self._path_property = path_property
        self._target_property = target_property

    def requires(self):
        return [self._path_property]

    def provides(self):
        return self._target_property

    def get_value(self, requirements, property_name):

        size = os.path.getsize(requirements[self._path_property])
        return size


class FileName(TingProperty):
    def __init__(self, path_property, target_property):

        self._path_property = path_property
        self._target_property = target_property

    def requires(self):
        return [self._path_property]

    def provides(self):
        return self._target_property

    def get_value(self, requirements, property_name):

        # print("FILE_NAME: {}".format(property_name))
        # import pp
        # pp(requirements)

        name = os.path.basename(requirements[self._path_property])
        return name


class PathParts(TingProperty):
    def __init__(
        self,
        path_property: str,
        target_property: str,
        include_extension=True,
        include_base_name=True,
        include_dir_name=True,
        repl_map=None,
    ):

        self._path_property = path_property
        self._target_property = target_property
        self._include_extension = include_extension
        self._include_base_name = include_base_name
        self._include_dir_name = include_dir_name
        if not repl_map:
            repl_map = {}
        self._repl_map = repl_map

    def provides(self) -> List[str]:
        return [self._target_property]

    def requires(self):
        return [self._path_property]

    def get_value(self, requirements: Dict[str, Any], property_name):

        path = requirements[self._path_property]
        result = []

        base_path, file_extension = os.path.splitext(path)
        dir = os.path.dirname(base_path)
        filename = os.path.basename(base_path)

        if self._include_dir_name:
            result.append(dir)
        if self._include_base_name:
            if result:
                result.append(os.path.sep)
            result.append(filename)
        if self._include_extension:
            result.append("." + file_extension)

        result = "".join(result)

        for k, v in self._repl_map.items():
            result = result.replace(k, v)

        return result


class TextContent(TingProperty):
    def __init__(self, path_property, target_property=None):

        self._path_property = path_property
        self._content_target = target_property

    def requires(self) -> List[str]:

        return self._path_property

    def provides(self) -> List[str]:

        return [self._content_target]

    def get_value(self, requirements, property_name):

        path = requirements[self._path_property]
        with open(path, "r") as f:
            content = f.read()

        return content
