[![PyPI status](https://img.shields.io/pypi/status/tings.svg)](https://pypi.python.org/pypi/tings/)
[![PyPI version](https://img.shields.io/pypi/v/tings.svg)](https://pypi.python.org/pypi/tings/)
[![PyPI pyversions](https://img.shields.io/pypi/pyversions/tings.svg)](https://pypi.python.org/pypi/tings/)
[![Pipeline status](https://gitlab.com/frkl/tings/badges/develop/pipeline.svg)](https://gitlab.com/frkl/tings/pipelines)
[![Code style](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/ambv/black)

# tings

*Create Python objects from things.*


## Description

Documentation still to be done.

# Development

Assuming you use [pyenv](https://github.com/pyenv/pyenv) and [pyenv-virtualenv](https://github.com/pyenv/pyenv-virtualenv) for development, here's how to setup a 'tings' development environment manually:

    pyenv install 3.7.3
    pyenv virtualenv 3.7.3 tings
    git clone https://gitlab.com/frkl/tings
    cd <tings_dir>
    pyenv local tings
    pip install -e .[all-dev]
    pre-commit install


## Copyright & license

Please check the [LICENSE](/LICENSE) file in this repository (it's a short license!), also check out the [*freckles* license page](https://freckles.io/license) for more details.

[Parity Public License 6.0.0](https://licensezero.com/licenses/parity)

[Copyright (c) 2019 frkl OÜ](https://frkl.io)
