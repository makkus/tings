# -*- coding: utf-8 -*-
import logging
import os
from abc import ABCMeta, abstractmethod
from typing import Type, Dict

from frtls.exceptions import FrklException
from plugtings.core import Plugting, EntryPointIndex
from tings.tings import TingsArchive, Tings

log = logging.getLogger("tings")


class TingsFreezerPlugting(Plugting):
    def __int__(self):

        index = EntryPointIndex(entry_point="tings.freezers")
        super(TingsFreezerPlugting, self).__init__(index=index)

    def get_base_class(self) -> Type:

        return TingsFreezer

    def get_default_plugin(self) -> str:

        return "json"


class TingsFreezer(metaclass=ABCMeta):
    def freeze(self, tings: Tings, metadata: Dict, path: str, force: bool = False):

        if path is not None and os.path.exists(path) and not force:
            raise FrklException(
                msg="Can't freeze tings.", reason=f"File {path} already exists"
            )

        archive = tings.export(metadata, include_no_cache_properties=False)
        self._freeze(archive, path=path, force=force)

    def unfreeze(self, target_class_name, path) -> "Tings":
        archive = self._unfreeze(path)

        from tings.tings import Tings

        tings_obj = Tings.create_from_archive(
            archive=archive, target_class_name=target_class_name
        )
        return tings_obj

    @abstractmethod
    def _freeze(self, archive: TingsArchive, path, force=False):
        pass

    @abstractmethod
    def _unfreeze(self, path) -> Dict[str, Dict]:
        pass
