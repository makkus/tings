# -*- coding: utf-8 -*-
from pathlib import Path
from typing import Any

from tings.defaults import TINGS_DEFAULT_SPEC_FOLDER
from tings.finders.pluggable import (
    PluggableTingFinder,
    DEFAULT_TING_FINDER_FIND_DATA,
    DEFAULT_TING_SPEC_FIND_DATA,
)
from tings.spec import TingSpec
from tings.tings import Tings


class PluginTingFinderTings(Tings):
    @classmethod
    def create_plugin_finder(
        cls, finder_alias: str, find_data: Any = None
    ) -> PluggableTingFinder:

        pft = PluginTingFinderTings(find_data=find_data)
        finder = pft.get_ting(finder_alias)
        if finder is None:
            raise ValueError(f"No finder with alias: {finder_alias}")

        return finder

    def __init__(self, find_data=None):
        self._finder = PluggableTingFinder()
        spec = TingSpec.from_file(Path(TINGS_DEFAULT_SPEC_FOLDER) / "ting-finder.spec")
        super(PluginTingFinderTings, self).__init__(
            ting_spec=spec, default_index="alias"
        )
        if find_data is None:
            find_data = DEFAULT_TING_FINDER_FIND_DATA
        self._find_data = find_data
        self._finder.register_tings(self, find_data=self._find_data)


class SpecTings(Tings):
    @classmethod
    def create_spec(cls, spec_alias: str, find_data: Any = None) -> TingSpec:

        st = SpecTings(find_data=find_data)
        spec = st.get_ting(spec_alias)
        if spec is None:
            raise ValueError(f"No ting spec with alias: {spec_alias}")

        return spec.ting_spec

    def __init__(self, find_data: Any = None):
        self._finder = PluggableTingFinder()
        spec = TingSpec.from_file(Path(TINGS_DEFAULT_SPEC_FOLDER) / "ting-spec.spec")
        super(SpecTings, self).__init__(ting_spec=spec, default_index="alias")
        if find_data is None:
            find_data = DEFAULT_TING_SPEC_FIND_DATA
        self._find_data = find_data
        self._finder.register_tings(self, find_data=self._find_data)
