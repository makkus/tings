# -*- coding: utf-8 -*-
from typing import List, Dict, Any

from tings.defaults import TING_PROPERTY_CACHE_MISS_KEY
from tings.ting import Ting, TingPropertyFunc, MultiResult


class CachedTing(Ting):
    def __init__(self):

        self._cache = {}
        self._ting_on_("invalidate", self._invalidate_properties)

    def _invalidate_properties(self, **kwargs):

        for property in kwargs["properties"]:
            self._cache.pop(property, None)

    def _ting_available_property_values_(self) -> Dict[str, Any]:
        return self._cache

    def _ting_execute_(self, funcs: List[TingPropertyFunc]):

        for func in funcs:

            if (
                func.property_name in self._cache.keys()
                and self._cache[func.property_name] != TING_PROPERTY_CACHE_MISS_KEY
            ):
                result = self._cache[func.property_name]
                func.result = result
                continue

            result = func.execute()

            if isinstance(result, MultiResult):
                for k, v in result.results.items():
                    self._cache[k] = v
            else:
                self._cache[func.property_name] = result

    def _ting_set_properties_(self, prop_dict: Dict[str, Any]):
        """Set available properties in case the implementation of the `Ting` supports it.

        This is useful to pre-warm caches etc.
        """

        self._cache.update(prop_dict)
