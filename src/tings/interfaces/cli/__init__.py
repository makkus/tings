# -*- coding: utf-8 -*-
import sys
from pathlib import Path

import click

from frtls.cli.exceptions import handle_exc
from frtls.cli.logging import logzero_option
from tings import _create_tings, create_plugin_finder_ting, create_tings
from tings.bootstrap import PluginTingFinderTings, SpecTings
from tings.finders.pluggable import PluggableTingFinder
from tings.interfaces.cli.watch import TingWatcher
from tings.spec import TingSpec
from tings.tings import Tings


@click.group()
@logzero_option()
@click.pass_context
def cli(ctx):

    pass


@cli.command("freeze")
@click.option(
    "--output",
    "-o",
    help="the output path",
    type=click.Path(exists=False, dir_okay=False, writable=True, resolve_path=True),
    required=True,
)
@click.option(
    "--force", "-f", help="overwrite an possibly already existing file", is_flag=True
)
@click.option(
    "--properties",
    "-p",
    help="optional parameters to freeze",
    type=str,
    multiple=True,
    required=False,
)
@click.pass_context
@handle_exc
def freeze(ctx, properties, output, force):

    SPEC_FOLDER = Path("/home/markus/projects/new/tings/tests/resources/specs")
    spec = TingSpec.from_file(SPEC_FOLDER / "spec_2.spec")

    finder = PluggableTingFinder()
    tings = Tings(ting_spec=spec)
    find_data = {
        "plugins": [
            {
                "files": {
                    "config": {
                        "base_paths": ["/home/markus/temp/tings/applications"],
                        "matchers": [{"path-regex": {"regex": "\\.tempting$"}}],
                    }
                }
            },
            {
                "files": {
                    "config": {
                        "base_paths": ["/home/markus/temp/tings/devops"],
                        "matchers": [{"path-regex": {"regex": "\\.tempting$"}}],
                    }
                }
            },
        ]
    }

    # inputs_map = {"_path": "_seed_path"}
    finder.register_tings(tings, find_data=find_data)

    f_md = {"additional_properties": "*"}
    tings.freeze(path=output, force=force, **f_md)


@cli.command(name="unfreeze")
@click.argument("path")
@click.pass_context
def unfreeze(ctx, path):

    tings = Tings.unfreeze(path, id="example_tings_from_archive")
    import pp

    # for t in tings.tings:
    #     t._ting_invalidate()
    pp(tings.to_value_list())


@cli.command(name="watch")
@click.option("--spec", "-s", help="the ting spec alias")
@click.option("--finder", "-f", help="the finder alias", default="file-extension")
@click.option("--finder-args")
@click.option("--index", "-i", help="the index to use")
@click.option("--property", "-p", multiple=True)
@click.pass_context
def watch(ctx, spec, finder, finder_args, index, property):

    finder_args = {
        "base_paths": [
            "/home/markus/projects/new/tings/src/tings/resources/tings/types"
        ],
        "extension": "type",
    }
    finder = create_plugin_finder_ting(finder)
    tings = _create_tings(spec_alias=spec, finder=finder, finder_args=finder_args)

    tings.add_index(index)

    if not finder.ting_finder.supports_watch():
        click.echo(
            "Watching not supported, the ting finder or one of it's plugins have not implemented the required method(s)."
        )
        sys.exit()

    tw = TingWatcher(
        tings=tings, finder=finder.ting_finder, index=index, properties=property
    )
    tw.watch()


@cli.command(name="list-finders")
@click.pass_context
def list_finders(ctx):

    pft = PluginTingFinderTings()

    for f in pft.tings:
        print(f"{f.alias}:\n   args: {', '.join(f.input_schema.keys())}")


@cli.command(name="list-specs")
@click.pass_context
def list_specs(ctx):

    st = SpecTings()
    for s in st.tings:
        print(s.alias)


@cli.command(name="types")
@click.pass_context
def types(ctx):

    finder_args = {
        "base_paths": [
            "/home/markus/projects/new/tings/src/tings/resources/tings/types"
        ],
        "extension": "type",
    }
    finder = create_plugin_finder_ting("file-extension")
    tings = _create_tings(spec_alias="type", finder=finder, finder_args=finder_args)

    # for ting in tings.tings:
    #     click.secho(f"{ting.name}:", bold=True)
    #     for p in ["parent_type"]:
    #         click.echo(f"  {p}: {getattr(ting, p)}")

    tings.add_index("name")
    t = tings.get_ting("list_of_strings")
    t2 = tings.get_ting("list")
    # print(t.type_dict)
    # for t in tings.tings:
    #     print(f"{t.name}: {t.type_dict}")

    def callback(*args, **kwargs):
        print(f"Event: {kwargs['change_type']}")
        print(t2.type_dict)
        print(t.type_dict)
        # print(args)
        # print(kwargs)

    tings.add_observer("item_change", callback)

    finder.watch()
    # tw = TingWatcher(tings=tings, finder=finder.ting_finder, index="name", properties=["type_dict"])
    # tw.watch()


@cli.command("test")
@click.argument("input_value")
@click.pass_context
@handle_exc
def test(ctx, input_value):

    # si = SmartInput(input_value=input_value)
    # import pp
    # pp(si.content)

    tings = create_tings(
        "/home/markus/projects/new/bring/src/bring/resources/bring.tings"
    )

    print(tings)


if __name__ == "__main__":
    cli()
