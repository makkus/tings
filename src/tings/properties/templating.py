# -*- coding: utf-8 -*-
from typing import List, Any, Dict

from frtls.templating import get_template_schema, get_global_jinja_env
from tings.properties import TingProperty


class TemplateInputSchema(TingProperty):
    def __init__(
        self,
        source_property,
        target_property,
        jinja_env_delimiter_profile="default",
        jinja_env_type="default",
    ):

        self._source_property = source_property
        self._target_property = target_property

        self._jinja_env = get_global_jinja_env(
            delimiter_profile=jinja_env_delimiter_profile, env_type=jinja_env_type
        )

    def provides(self) -> List[str]:

        return [self._target_property]

    def requires(self) -> List[str]:

        return [self._source_property]

    def get_value(self, requirements: Dict[str, Any], property_name):

        schema = get_template_schema(
            requirements[self._source_property], self._jinja_env
        )
        return schema
