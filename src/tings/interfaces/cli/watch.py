# -*- coding: utf-8 -*-
import asyncio

import urwid


class TingWatcher(object):
    def __init__(self, tings, finder, properties=None, index=None):

        self._tings = tings
        self._finder = finder
        self._index = tings.get_index(index_name=index)
        if not properties:
            properties = tings.spec.ting_property_names
        self._watched_properties = properties
        self._main_loop = None

    def watch(self):

        loop = asyncio.get_event_loop()
        watch_tasks = self._finder.collect_watch_tasks()

        for tasks in watch_tasks.values():
            for task in tasks:
                func = task["func"]
                args = task.get("args")
                loop.create_task(func(*args))

        evl = urwid.AsyncioEventLoop(loop=loop)
        palette = [
            ("titlebar", "dark red", ""),
            ("quit button", "dark red", ""),
            ("getting quote", "dark blue", ""),
            ("headers", "white,bold", ""),
            ("change ", "dark green", ""),
            ("change negative", "dark red", ""),
        ]

        quote_text = urwid.Text("Loading...")
        quote_filler = urwid.Filler(quote_text, valign="top", top=1, bottom=1)
        v_padding = urwid.Padding(quote_filler, left=1, right=1)
        quote_box = urwid.LineBox(v_padding)
        header_text = urwid.Text(" Tings!")
        header = urwid.AttrMap(header_text, "titlebar")

        menu = urwid.Text(["Press (", ("quit button", "Q"), ") to quit."])

        def update(*args, **kwargs):
            self._main_loop.draw_screen()
            quote_box.base_widget.set_text(self.get_content())

        # Handle key presses
        def handle_input(key):
            if key == "R" or key == "r":
                update()

            if key == "Q" or key == "q":
                raise urwid.ExitMainLoop()

        self._tings.add_observer("item_change", update)
        # def refresh(_loop, _data):
        #     self._main_loop.draw_screen()
        #     quote_box.base_widget.set_text(self.get_content())
        #     self._main_loop.set_alarm_in(0.1, refresh)

        layout = urwid.Frame(header=header, body=quote_box, footer=menu)

        self._main_loop = urwid.MainLoop(
            layout, palette, unhandled_input=handle_input, event_loop=evl
        )
        self._main_loop.set_alarm_in(0, update)
        # self._main_loop.set_alarm_in(0, refresh)
        self._main_loop.run()

    def get_content(self):

        res = {}
        for ting_id, ting in self._index.items():
            res[ting_id] = {}
            for p in self._watched_properties:
                res[ting_id][p] = getattr(ting, p)

        result = []
        for ting_id, data in res.items():
            result.append(ting_id + ":")
            for k, v in data.items():
                result.append(f"  {k}: {v}")
            result.append("")

        return "\n".join(result)
