#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Tests for `tings` package."""
import os
from pathlib import Path

import pytest

from tings.defaults import TING_INPUT_NOT_SET
from tings.spec import TingSpec
from tings.ting import Ting, TingSeed

TEST_RESOURCES = Path(os.path.dirname(__file__), "resources")

SPEC_FOLDER = TEST_RESOURCES / "specs"


@pytest.fixture
def ting_resources():

    pass


def test_spec():

    spec = TingSpec.from_file(SPEC_FOLDER / "spec_1.spec")
    assert list(spec.ting_inputs.keys()) == ["_path"]


def test_spec_class():

    spec = TingSpec.from_file(SPEC_FOLDER / "spec_1.spec")

    test_cls = type("TestTing", (Ting,), {}, spec=spec)
    t1 = test_cls()

    for attr in [
        "_ting_available_property_values_",
        "_ting_execute_",
        "_ting_get_property_",
        "_ting_get_requirement_keys_",
        "_ting_get_requirement_values_for_property_",
        "_ting_input__path_",
        "_ting_input_dict_",
        "_ting_input_names_",
        "_ting_properties_",
        "_ting_property_names_",
        "_ting_retrieve_property_values_",
    ]:

        assert hasattr(t1, attr)

    # for attr in ["file_name", "file_size", "id"]:
    #     assert hasattr(t1, attr)


def test_spec_property_dependency():

    spec = TingSpec.from_file(SPEC_FOLDER / "spec_2.spec")

    test_cls = type("TestTing", (Ting,), {}, spec=spec)
    t1 = test_cls()
    spec_path = (SPEC_FOLDER / "spec_2.spec").as_posix()
    t1._seed_path = spec_path
    assert t1.file_name_mirror == "spec_2.spec"


def test_ting_seed():

    spec = TingSpec.from_file(SPEC_FOLDER / "spec_2.spec")

    test_cls = type("TestTing", (Ting,), {}, spec=spec)
    t1 = test_cls()

    spec_path = (SPEC_FOLDER / "spec_2.spec").as_posix()

    seed = TingSeed.from_ting(t1)
    assert seed._ting._ting_input_names_ == ["_seed_path"]

    seed.set_seed_input_value(_seed_path=spec_path)
    assert t1.file_name_mirror == "spec_2.spec"


def test_attributes():

    spec = TingSpec.from_file(SPEC_FOLDER / "spec_1.spec")

    test_cls = type("TestTing", (Ting,), {}, spec=spec)
    t1 = test_cls()

    assert t1._path == TING_INPUT_NOT_SET

    spec_path = (SPEC_FOLDER / "spec_1.spec").as_posix()
    t1._path = spec_path
    assert t1._path == spec_path
    assert isinstance(t1.file_size, int) and t1.file_size > 0
    assert t1.file_name == "spec_1.spec"


def test_observable():

    spec = TingSpec.from_file(SPEC_FOLDER / "spec_1.spec")

    test_cls = type("TestTing", (Ting,), {}, spec=spec)
    t1 = test_cls()
    assert hasattr(t1, "_ting_observable_")
    assert hasattr(t1, "_ting_observable_obj_")
    t2 = test_cls()
    assert t1._ting_observable_ != t2._ting_observable_

    # input_set = False

    # def callback(**kwargs):
    #     nonlocal input_set
    #     input_set = True

    property_invalidated = False

    def callback_2(**kwargs):
        nonlocal property_invalidated

        if "file_size" in kwargs["properties"]:
            property_invalidated = True

    # t1._ting_on_("ting", callback)
    t1._ting_on_("invalidate", callback_2)
    t1._path = (SPEC_FOLDER / "spec_1.spec").as_posix()
    # assert input_set
    assert property_invalidated
