# -*- coding: utf-8 -*-
import copy
import uuid
from collections import Sequence
from typing import Dict, List, Any, Mapping

import dpath

from frtls.formats import auto_parse_string, determine_input_type, get_content
from tings.defaults import NO_CACHE_KEY
from tings.properties import TingProperty


class StaticDict(TingProperty):
    """A property that contains a static, pre-defined dictionary.

    Args:

        - target_property_name: the name of the property
        - dict_data: the dictionary payload
    """

    def __init__(self, target_property: str, dict_data: Dict):

        self._target_property_name = target_property
        self._dict_data = dict_data

    def provides(self):
        return [self._target_property_name]

    def get_value(self, requirements, property_name):

        print("STATIC DICT")
        print(f"self: {self}")

        return self._dict_data


class DictValue(TingProperty):
    """A property to return a sub-element of a dictionary.

    """

    def __init__(
        self,
        source_property: str,
        key: str,
        target_property: str = None,
        default={},
        copy_value=False,
    ):

        self._source_property_name = source_property
        self._key = key
        if target_property is None:
            target_property = self._key.replace(".", "_")
        self._target_property_name = target_property
        self._default = default
        self._copy_value = copy_value

    def requires(self) -> List[str]:

        return self._source_property_name

    def provides(self) -> List[str]:

        return self._target_property_name

    def get_value(self, requirements, property_name):

        data_dict = requirements[self._source_property_name]
        try:
            result = dpath.util.get(data_dict, self._key, separator=".")
            if self._copy_value:
                result = copy.deepcopy(result)
        except (KeyError):
            result = copy.deepcopy(self._default)

        return result


class DictContent(TingProperty):
    def __init__(
        self, source_property, target_property=None, content_string_format="auto"
    ):

        self._text_property = source_property
        self._dict_content_target = target_property
        self._content_string_format = content_string_format

    def requires(self) -> List[str]:

        return self._text_property

    def provides(self) -> List[str]:

        return [self._dict_content_target]

    def get_value(self, requirements, property_name):

        text = requirements[self._text_property]
        result = auto_parse_string(
            content=text, content_type=self._content_string_format, default_if_empty={}
        )
        return result


class DictMerge(TingProperty):
    def __init__(self, source_properties, target_property):

        self._source_properties = source_properties
        self._target_property = target_property

    def provides(self) -> List[str]:

        return [self._target_property]

    def requires(self) -> List[str]:

        return self._source_properties

    def get_value(self, requirements: Dict[str, Any], property_name):

        sources = []
        for s in self._source_properties:
            sources.append(requirements[s])

        if sources is None:
            return {}

        src = []
        for s in sources:
            if not s:
                continue
            if isinstance(s, Sequence):
                for temp in s:
                    if not isinstance(temp, Mapping):
                        raise Exception(
                            f"Can't merge dictionaries, invalid type for item: {temp}"
                        )
                    src.append(temp)
            else:
                src.append(s)

        result = {}
        for s in src:
            dpath.merge(result, s)

        return result


class Id(TingProperty):
    """A property to give a `ting` an Id property.

    NOTE: this property by default is not cached. let me know if that is a problem
    """

    VALID_ID_TYPES = ["uuid", "counter"]
    GLOBAL_COUNTER = 0

    def __init__(self, id_property="id", id_type="uuid"):

        self._id_property = id_property
        if id_type not in Id.VALID_ID_TYPES:
            raise Exception(f"Invalid id type, use one of: {Id.VALID_ID_TYPES}")
        self._id_type = id_type
        if id_type == "uuid":
            self._id = str(uuid.uuid4())
        else:
            self._id = Id.GLOBAL_COUNTER
            Id.GLOBAL_COUNTER = Id.GLOBAL_COUNTER + 1

    def metadata(self):

        return {NO_CACHE_KEY: self._id_property}

    def provides(self):

        return [self._id_property]

    def get_value(self, requirements, property_name):

        return self._id


class Mirror(TingProperty):
    def __init__(self, source_property: str, target_property: str = None):
        """Mirrors another property (most likely a ting arg).

        If no `target_property` is provided, the `source_property` value without leading '_' is used. If `source_property`
        does not start with '_', an error is thrown.

        Args:
            - *source_property*: the source property name
            - *target_property*: the target property name
        """

        self._source_property = source_property
        if target_property is None:
            if source_property.startswith("_"):
                target_property = source_property[1:]
            else:
                raise ValueError(
                    "No 'target_property' provided, and 'source_property' does not start with '_'."
                )
        self._target_property = target_property

    def metadata(self):
        return {NO_CACHE_KEY: [self._target_property]}

    def requires(self):
        return [self._source_property]

    def provides(self) -> List[str]:
        return [self._target_property]

    def get_value(self, requirements, property_name):
        return requirements[self._source_property]


class InputType(TingProperty):
    def __init__(self, source_property: Any, target_property: str = "input_type"):

        self._source_property = source_property
        self._target_property = target_property

    def requires(self) -> List[str]:

        return [self._source_property]

    def provides(self) -> List[str]:

        return [self._target_property]

    def get_value(self, requirements: Dict[str, Any], property_name):

        input_obj = requirements[self._source_property]

        ift = determine_input_type(input_obj)
        if ift is None:
            raise ValueError(f"Can't determine input type for: {input_obj}")

        return ift


class InputContent(TingProperty):
    def __init__(self, source_property, input_type_property, target_property):

        self._source_property = source_property
        self._input_type_property = input_type_property
        self._target_property = target_property

    def requires(self) -> List[str]:

        return [self._source_property, self._input_type_property]

    def provides(self) -> List[str]:

        return [self._target_property]

    def get_value(self, requirements: Dict[str, Any], property_name):

        input_type = requirements[self._input_type_property]
        input_content = requirements[self._source_property]

        content = get_content(input_content, input_type=input_type)

        return content
