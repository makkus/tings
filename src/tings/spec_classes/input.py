# -*- coding: utf-8 -*-

from tings import SpecTings
from tings.defaults import TING_INPUT_NOT_SET
from tings.ting import Ting

SMART_INPUT_SPEC = SpecTings.create_spec("smart-input")


class SmartInputTing(Ting, spec=SMART_INPUT_SPEC):
    def __init__(self, _input=TING_INPUT_NOT_SET):

        if input != TING_INPUT_NOT_SET:
            self._input = _input
