# -*- coding: utf-8 -*-
import json
from typing import Dict

from tings.freezers import TingsFreezer
from tings.tings import TingsArchive


class JsonFreezer(TingsFreezer):
    def _freeze(self, archive: TingsArchive, path, force=False):

        json_string = json.dumps(archive.to_dict())

        with open(path, "w", encoding="utf-8") as f:
            f.write(json_string)

    def _unfreeze(self, path) -> Dict[str, Dict]:

        with open(path, "r", encoding="utf-8") as f:
            frozen_tings = json.load(f)

        archive = TingsArchive.from_dict(frozen_tings)
        return archive
