# -*- coding: utf-8 -*-

import io
import os

from pkg_resources import get_distribution, DistributionNotFound

from frtls.formats import SmartInput
from tings.bootstrap import PluginTingFinderTings, SpecTings
from tings.finders import TingFindersPlugting
from tings.spec import TingSpec
from tings.tings import Tings

"""Top-level package for tings."""

__author__ = """Markus Binsteiner"""
__email__ = "markus@frkl.io"

try:
    # Change here if project is renamed and does not equal the package name
    dist_name = __name__
    __version__ = get_distribution(dist_name).version
except DistributionNotFound:

    try:
        version_file = os.path.join(os.path.dirname(__file__), "version.txt")

        if os.path.exists(version_file):
            with io.open(version_file, encoding="utf-8") as vf:
                __version__ = vf.read()
        else:
            __version__ = "unknown"

    except (Exception):
        pass

    if __version__ is None:
        __version__ = "unknown"

finally:
    del get_distribution, DistributionNotFound


def create_plugin_finder_ting(finder_alias, find_data=None):

    finder = PluginTingFinderTings.create_plugin_finder(
        finder_alias=finder_alias, find_data=None
    )
    return finder


def _create_tings(spec_alias, finder, finder_args=None):

    if isinstance(finder, str):
        finder = create_plugin_finder_ting(finder_alias=finder)

    if finder_args is None:
        finder_args = {}

    spec = SpecTings.create_spec(spec_alias=spec_alias)

    tings = finder.create_tings(ting_spec=spec, **finder_args)

    return tings


def create_tings(source):

    src = SmartInput(input_value=source, force_content="dict")
    finder_conf = src.content.get("finder", "plugin-finder")
    find_data = src.content.get("find_data")
    spec = src.content.get("spec")

    pt = TingFindersPlugting()
    finder = pt.create_obj(init_data=finder_conf)

    spec = TingSpec.from_dict(spec)

    tings = Tings(ting_spec=spec)
    finder.register_tings(tings, find_data=find_data)

    return tings
