# -*- coding: utf-8 -*-
import os
import re
from typing import List, Callable

from watchgod import DefaultDirWatcher


class PathRegexMatcher(object):
    def __init__(self, regex, match_full_path=False):

        self._regex = regex
        self._match_full_path = match_full_path

    def match(self, path):

        if self._match_full_path:
            text = path
        else:
            text = os.path.basename(path)
        return re.search(self._regex, text)


def file_matches(path: str, matcher_list: List[Callable] = None) -> bool:

    if not matcher_list:
        return True

    for matcher in matcher_list:
        match = matcher.match(path)
        if not match:
            return False

    return True


class TingFileWatcher(DefaultDirWatcher):
    def __init__(self, root_path, matchers):

        self._matchers = matchers
        super(TingFileWatcher, self).__init__(root_path=root_path)

    def should_watch_file(self, entry):

        result = file_matches(entry.path, matcher_list=self._matchers)
        return result
