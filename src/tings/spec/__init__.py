# -*- coding: utf-8 -*-
from pathlib import Path
from typing import List, Union, Dict, Mapping, Type, Any

from frtls.classes import generate_valid_identifier
from frtls.formats import auto_parse_string
from frtls.lists import ensure_sequence, FrklList
from tings.addons import TingAddonsPlugting
from tings.properties import TingPropertiesPlugting, TingProperty
from tings.utils.utils import find_all_dependencies


class TingSpec(object):
    """A class to describe the properties and input of a `Ting` class.

    Used in the `TingMeta` custom metaclass to assemble the class.
    """

    @classmethod
    def from_file(cls, path: Path, content_type="auto"):

        content = path.read_text()

        spec_dict = auto_parse_string(
            content=content, content_type=content_type, content_origin=path
        )

        return cls.from_dict(spec_dict)

    @classmethod
    def from_dict(cls, data):

        return TingSpec(**data)

    def __init__(
        self,
        properties: List[Union[Dict, str]] = None,
        addons: List[str] = None,
        extra_input: List[str] = None,
        ting_class_name: str = None,
        ting_base_class: Type = None,
    ):

        if not properties:
            properties = []
        if ting_class_name is None:
            ting_class_name = generate_valid_identifier(first_character_uppercase=True)

        self._extra_input = ensure_sequence(extra_input, convert_none=True)

        self._mixins_plugting = TingAddonsPlugting()
        if addons is None:
            addons = []
        self._addons = FrklList(
            *addons, key="_type", allow_regular_dict_items=True
        ).export()

        self._ting_addons = self._load_addons(self._addons)
        self._ting_class_name = ting_class_name
        self._ting_cls = None

        if ting_base_class is None:
            from tings.ting.cached import CachedTing

            ting_base_class = CachedTing
        # TODO: convert string input
        self._ting_base_class = ting_base_class

        self._default_cls = None

        self._properties_plugting = TingPropertiesPlugting()
        self._properties = FrklList(
            *properties, key="_type", allow_regular_dict_items=True
        ).export()

        (
            self._ting_properties,
            self._ting_property_map,
            self._ting_property_dependencies,
            self._ting_inputs,
        ) = self._load_properties(self._properties)

        self._ting_property_names = []
        for p in self.ting_properties:
            prov = p._provides_check()
            self._ting_property_names.extend(prov)

    @property
    def ting_class_name(self):
        return self._ting_class_name

    @property
    def ting_class(self):

        if self._ting_cls is None:
            self._ting_cls = type(
                self._ting_class_name, (self._ting_base_class,), {}, spec=self
            )
        return self._ting_cls

    @property
    def ting_base_class(self):
        return self._ting_base_class

    def _load_addons(self, addons: List[str]):

        result = []

        for addon in addons:
            addon_cls, init_data = self._mixins_plugting.create_plugin_class(
                init_data=addon
            )
            result.append({"cls": addon_cls, "init_data": init_data})

        return result

    def _load_properties(
        self, properties: List[Union[Dict, str]]
    ) -> List[TingProperty]:

        property_objs = []
        property_map = {}
        for prop in properties:
            if isinstance(prop, str):
                prop = {"_type": prop}
            elif not isinstance(prop, Mapping):
                raise Exception(
                    f"Invalid property item, needs to be mapping or string: {prop}"
                )
            elif "_type" not in prop.keys():
                raise Exception(f"Invalid property item, needs the '_type' key.")

            prop_obj = self._properties_plugting.create_obj(init_data=prop)
            property_objs.append(prop_obj)

            for prov in prop_obj._provides_check():

                if prov in property_map.keys():
                    raise ValueError(
                        f"Can't assemble Ting class, duplicate property '{prov}'"
                    )
                property_map[prov] = prop

        missing = set()
        dependencies = {}
        for prop_obj in property_objs:

            for req in prop_obj._requires_check():
                if req not in property_map.keys():
                    missing.add(req)

                for prov in prop_obj._provides_check():
                    dependencies.setdefault(prov, []).append(req)

        property_dependencies = {}
        for k, v in dependencies.items():
            deps = find_all_dependencies(k, dependency_map=dependencies)
            property_dependencies[k] = deps

        input_dependencies = {}
        for m in missing:
            deps = find_all_dependencies(m, dependency_map=dependencies)
            input_dependencies[m] = deps

        for ei in self._extra_input:
            if ei not in input_dependencies.keys():
                input_dependencies[ei] = []

        return property_objs, property_map, property_dependencies, input_dependencies

    def to_dict(self):

        return {
            "properties": self._properties,
            "extra_input": self._extra_input,
            "ting_class_name": self._ting_class_name,
        }

    @property
    def ting_properties(self) -> List[TingProperty]:
        return self._ting_properties

    @property
    def ting_addons(self) -> List[Dict[str, Any]]:
        return self._ting_addons

    @property
    def ting_inputs(self) -> Dict[str, List[str]]:

        return self._ting_inputs

    @property
    def ting_input_names(self) -> List[str]:

        return list(self._ting_inputs.keys())

    @property
    def ting_property_names(self) -> List[str]:

        return self._ting_property_names

    @property
    def ting_property_dependencies(self) -> Dict[str, List[str]]:

        return self._ting_property_dependencies

    @property
    def default_ting_class(self):

        if self._default_cls is None:
            self._default_cls = self.build_class()
        return self._default_cls

    def build_class(self, ting_class_name=None, ting_base_class=None):

        if ting_class_name is None:
            ting_class_name = self._ting_class_name
        if ting_base_class is None:
            ting_base_class = self._ting_base_class

        hierarchy = (ting_base_class,)
        cls = type(ting_class_name, hierarchy, {}, spec=self)
        return cls
