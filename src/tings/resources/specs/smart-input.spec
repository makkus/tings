properties:
  - mirror:
      source_property: _input
      target_property: input
  - input-type:
      source_property: input
  - input-content:
      source_property: _input
      input_type_property: input_type
      target_property: content
