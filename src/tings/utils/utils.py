# -*- coding: utf-8 -*-
from typing import List, Dict


def find_all_dependencies(
    key: str, dependency_map: Dict[str, List[str]], current_key_list: List[str] = None
) -> List[str]:

    if current_key_list is None:
        current_key_list = []

    for k, v in dependency_map.items():

        if key in v and k not in current_key_list:
            current_key_list.append(k)
            all_deps = find_all_dependencies(
                k, dependency_map=dependency_map, current_key_list=current_key_list
            )
            for d in all_deps:
                if d not in current_key_list:
                    current_key_list.append(d)

    return current_key_list
