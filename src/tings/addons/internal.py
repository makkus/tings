# -*- coding: utf-8 -*-
from frtls.templating import replace_strings_in_obj, get_global_jinja_env
from tings.addons import TingAddon
from tings.finders import TingFinder
from tings.tings import Tings


class CreateFindData(TingAddon):
    def provides(self):

        return ["create_find_data"]

    def requires(self):

        return ["find_data", "input_schema"]

    def create_find_data(self, **finder_args):

        find_data = self.get_requirement("find_data")

        # schema = self.get_requirement("input_schema")

        find_data_final = replace_strings_in_obj(
            find_data,
            replacement_dict=finder_args,
            jinja_env=get_global_jinja_env(delimiter_profile="frkl", env_type="native"),
        )

        return find_data_final


class CreateTings(TingAddon):
    def create_tings(self, ting_spec, **finder_args):

        finder: TingFinder = self.get_requirement("ting_finder")

        find_data_final = self.get_requirement("create_find_data")(**finder_args)

        tings = Tings(ting_spec=ting_spec)
        finder.register_tings(tings, find_data=find_data_final)

        return tings

    def provides(self):
        return ["create_tings"]

    def requires(self):

        return ["ting_finder", "create_find_data"]


class WatchTings(TingAddon):
    def provides(self):

        return ["watch"]

    def requires(self):

        return ["ting_finder"]

    def watch(self, *event_handlers):

        finder = self.get_requirement("ting_finder")

        finder.watch(*event_handlers)
