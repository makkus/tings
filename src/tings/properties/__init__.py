# -*- coding: utf-8 -*-
from abc import ABCMeta, abstractmethod
from typing import List, Type, Any, Dict

from frtls.lists import is_list_of_strings
from plugtings.core import Plugting, EntryPointIndex


class TingPropertiesPlugting(Plugting):
    def __init__(self):

        index = EntryPointIndex(entry_point="tings.properties")
        super(TingPropertiesPlugting, self).__init__(index)

    def get_base_class(self) -> Type:

        return TingProperty

    def get_default_init_data(self, alias):

        return None


class TingProperty(metaclass=ABCMeta):
    """Base class that constains the function that will become a Ting object property.

    It also contains the metadata that is required to add the property to the Ting object (for example the names
    of other Ting properties that are used within the function.
    """

    # def __repr__(self):
    #     return f"[TingProperty: provides={self.provides}, requires={self.requires}]"

    def _metadata_check(self):
        if hasattr(self, "metadata"):
            return self.metadata()
        else:
            return {}

    def _provides_check(self):

        prov = self.provides()
        if isinstance(prov, str):
            return [prov]
        elif is_list_of_strings(prov):
            return prov
        else:
            return ValueError(
                f"Invalid 'provides' value for TingAttribute '{self.__class__.__name__}', must be string or list of strings.",
                prov,
            )

    @abstractmethod
    def provides(self) -> List[str]:
        """Return the names of the properties this TingProperty provides."""

        pass

    @abstractmethod
    def get_value(self, requirements: Dict[str, Any], property_name):

        pass

    def _requires_check(self):

        req = self.requires()

        if isinstance(req, str):
            return [req]
        elif is_list_of_strings(req):
            return req
        else:
            return ValueError(
                f"Invalid 'requires' value for TingAttribute '{self.__class__.__name__}', must be string or list of strings.",
                req,
            )

    def requires(self) -> List[str]:

        return []


class SimpleTingProperty(TingProperty):
    def __init__(self, source_property, target_property):
        self._source_property = source_property
        self._target_property = target_property

    def provides(self) -> List[str]:
        return [self._target_property]

    def requires(self) -> List[str]:
        return [self._source_property]

    @abstractmethod
    def get_value(self, requirements: Dict[str, Any], property_name):
        pass
