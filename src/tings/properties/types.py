# -*- coding: utf-8 -*-
import copy
from typing import List, Dict, Any

import dpath

from tings.defaults import TINGS_INPUT_KEY
from tings.properties import TingProperty
from tings.ting import TingCallbackInput

NO_PARENT_TYPE_MARKER = "__no_parent_type__"


class ParentType(TingProperty):
    def __init__(self, source_property, target_property="parent_type"):

        self._source_property = source_property
        self._target_property = target_property

    def requires(self) -> List[str]:

        return [self._source_property]

    def provides(self) -> List[str]:

        return [self._target_property]

    def get_value(self, requirements: Dict[str, Any], property_name):

        parent_type = requirements[self._source_property].get(
            "type", NO_PARENT_TYPE_MARKER
        )
        return parent_type


class TypeChain(TingProperty):
    def __init__(self, source_property, target_property="type_chain"):

        self._source_property = source_property
        self._target_property = target_property

    def requires(self) -> List[str]:

        return [TINGS_INPUT_KEY, self._source_property]

    def provides(self) -> List[str]:

        return [self._target_property]

    def get_value(self, requirements: Dict[str, Any], property_name):

        data = copy.copy(requirements[self._source_property])
        parent_type = data.pop("type", None)

        tings: TingCallbackInput = requirements[TINGS_INPUT_KEY]

        chain = [data]
        if parent_type is not None:
            parent = tings(f"name.{parent_type}")
            if parent is None:
                raise Exception(f"No parent type: {parent_type}")
            parent_chain = getattr(parent, self._target_property)
            chain.extend(parent_chain)

        # print(chain)
        return chain


class TypeDict(TingProperty):
    def __init__(self, source_property="dict_chain", target_property="type_dict"):

        self._source_property = source_property
        self._target_property = target_property

    def requires(self) -> List[str]:

        return [self._source_property]

    def provides(self) -> List[str]:

        return [self._target_property]

    def get_value(self, requirements: Dict[str, Any], property_name):

        dict_chain: List = requirements[self._source_property]
        result = {}
        for d in reversed(dict_chain):
            dpath.util.merge(result, d)

        return result
