properties:
  - id
  - file-size:
      path_property: _seed_path
      target_property: file_size
  - file-name:
      path_property: _seed_path
      target_property: file_name
  - mirror:
      source_property: file_name
      target_property: file_name_mirror

addons:
  - to-dict
