# -*- coding: utf-8 -*-
from typing import Dict, Any, List

from tings.finders import TingFindersPlugting
from tings.properties import TingProperty
from tings.spec import TingSpec
from tings.ting import MultiResult


class TingFinderObj(TingProperty):
    def __init__(self, source_property, finder_obj_property, find_data_property):
        self._source_property = source_property
        self._finder_target_property = finder_obj_property
        self._find_data_property = find_data_property
        self._plugting = TingFindersPlugting()

    def provides(self) -> List[str]:
        return [self._finder_target_property, self._find_data_property]

    def requires(self) -> List[str]:
        return [self._source_property]

    def get_value(self, requirements: Dict[str, Any], property_name):

        finder_conf = requirements[self._source_property]
        finder_type = finder_conf["type"]

        finder = self._plugting.create_obj(entry_point=finder_type)
        find_data = finder_conf["find_data"]

        return MultiResult(
            **{
                self._finder_target_property: finder,
                self._find_data_property: find_data,
            }
        )


class TingSpecObj(TingProperty):
    def __init__(self, source_property, target_property):

        self._source_property = source_property
        self._target_property = target_property

    def provides(self) -> List[str]:

        return [self._target_property]

    def requires(self) -> List[str]:

        return [self._source_property]

    def get_value(self, requirements: Dict[str, Any], property_name):

        spec_dict = requirements[self._source_property]

        spec = TingSpec.from_dict(spec_dict)

        return spec
