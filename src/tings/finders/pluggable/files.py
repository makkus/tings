# -*- coding: utf-8 -*-
import os
from typing import List

from watchgod import Change, awatch

from frtls.defaults import DEFAULT_EXCLUDE_DIRS
from frtls.lists import ensure_sequence, FrklList
from plugtings.core import ensure_object_list
from tings.defaults import SEED_PATH_KEY, REL_SEED_PATH_KEY
from tings.finders.pluggable import TingFinderPlugin, PluggableTingSeedBuilder
from tings.utils.files import file_matches, TingFileWatcher


class FileTingFinderPlugin(TingFinderPlugin):
    def __init__(self, finder, tings_id, config):

        super(FileTingFinderPlugin, self).__init__(
            finder=finder, tings_id=tings_id, config=config
        )

        base_paths = config.get("base_paths", None)
        if not base_paths:
            base_paths = [os.path.abspath(".")]
        self._base_paths = ensure_sequence(base_paths)

        matchers = config.get("matchers", [])
        matchers = FrklList.from_dict({"items": matchers, "key": "_type"})
        self._matchers = ensure_object_list("tings.utils.files.matchers", matchers)

        self._recursive = config.get("recursive", True)

    def provides(self) -> List[str]:
        return [SEED_PATH_KEY, REL_SEED_PATH_KEY]

    def find_items(self, builder):

        result = {}
        for base_path in self._base_paths:
            for root, dirnames, filenames in os.walk(base_path, topdown=True):

                dirnames[:] = [d for d in dirnames if d not in DEFAULT_EXCLUDE_DIRS]

                for filename in [
                    f
                    for f in filenames
                    if file_matches(
                        path=os.path.join(root, f), matcher_list=self._matchers
                    )
                ]:

                    temp = os.path.join(root, filename)
                    rel = os.path.relpath(temp, base_path)
                    seed_item = {SEED_PATH_KEY: temp, REL_SEED_PATH_KEY: rel}
                    builder.seed_added(seed_id=temp, seed=seed_item)

        return result

    def watch_tasks(self, builder: PluggableTingSeedBuilder):
        def process_change(base_path, change, path):

            rel = os.path.relpath(path, base_path)
            item = {SEED_PATH_KEY: path, REL_SEED_PATH_KEY: rel}

            if change == Change.added:
                builder.seed_added(seed_id=path, seed=item)
            elif change == Change.deleted:
                builder.seed_removed(seed_id=path)
            elif change == Change.modified:
                builder.seed_updated(seed_id=path, seed=item)

        def process_changes(base_path, changes):

            for c in changes:
                process_change(base_path, *c)

        # TODO: maybe also provide alternative implementation that uses Watchdog, which has inotify support
        tasks = []

        async def watch(path, matchers):
            async for changes in awatch(
                path, watcher_cls=TingFileWatcher, watcher_kwargs={"matchers": matchers}
            ):
                process_changes(path, changes)

        for path in self._base_paths:
            wt = {"func": watch, "args": [path, self._matchers]}
            tasks.append(wt)
        return tasks
