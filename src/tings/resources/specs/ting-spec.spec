ting_class_name: TingSpecTing
properties:
  - mirror:
      source_property: _id
      target_property: alias
  - mirror:
      source_property: _dict_content
      target_property: dict_content
  - ting-spec-obj:
      source_property: dict_content
      target_property: ting_spec
