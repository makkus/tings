# -*- coding: utf-8 -*-
import uuid
from typing import List, Any

from tings.finders import TingFinder, TingSeed


class ArchiveTingFinder(TingFinder):
    def __init__(self):

        super(ArchiveTingFinder, self).__init__()

        self._archives = {}

    def _register_tings(self, tings_id: str, find_data: Any, input_names: List[str]):

        self._archives.setdefault(tings_id, []).append(
            {"archive": find_data, "input_names": input_names}
        )

    def _get_seeds(self, tings_id: str):

        archives = self._archives[tings_id]

        for archive_data in archives:
            archive = archive_data["archive"]
            input_names = archive_data["input_names"]
            for ting in archive.ting_list:
                seed_dict = ting["seed"]
                id = str(uuid.uuid4())
                seed = TingSeed(id=id)

                input_values = {}
                for i_n in input_names:
                    input_values[i_n] = seed_dict[i_n]

                seed.set_seed_input_value(**input_values)

                p = ting.get("properties", None)

                self.seed_added(seed=seed, tings_id=tings_id, cached_values=p)
