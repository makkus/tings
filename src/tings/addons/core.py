# -*- coding: utf-8 -*-

from tings.addons import TingAddon


class ToDict(TingAddon):
    def __init__(self, ting, attribute_names=None):

        if attribute_names is None:
            attribute_names = ting._ting_property_names_ + ting._ting_input_names_
        self._attribute_names = attribute_names

        super(ToDict, self).__init__(ting=ting)

    def attributes_dict(self):

        return self.get_requirements()

    def provides(self):

        return ["attributes_dict"]

    def requires(self):

        return self._attribute_names
