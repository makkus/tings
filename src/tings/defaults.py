# -*- coding: utf-8 -*-
import os
import sys

from appdirs import AppDirs

from frtls.templating import get_global_jinja_env

tsks_app_dirs = AppDirs("tings", "frkl")

FROZEN_TING_SPEC_KEY = "ting_spec"
"""The key that denotes the spec in a frozen tings archive."""
FROZEN_TING_LIST_KEY = "ting_list"
"""The key that denotes the list of frozen ting items in a frozen tings archive."""
FROZEN_TING_METADATA_KEY = "metadata"
"""The key that denotes the metadata contained in a frozen tings archive."""

TING_PROPERTY_CACHE_MISS_KEY = "__ting_cache_miss__"
"""Marker to indicate a value is not in the cache."""

TING_INPUT_NOT_SET = "__ting_input_not_set__"

if not hasattr(sys, "frozen"):
    TINGS_MODULE_BASE_FOLDER = os.path.dirname(__file__)
    """Marker to indicate the base folder for the `tings` module."""
else:
    TINGS_MODULE_BASE_FOLDER = os.path.join(sys._MEIPASS, "tings")
    """Marker to indicate the base folder for the `tings` module."""
TINGS_RESOURCES_FOLDER = os.path.join(TINGS_MODULE_BASE_FOLDER, "resources")

TINGS_DEFAULT_SPEC_FOLDER = os.path.join(TINGS_MODULE_BASE_FOLDER, "resources", "specs")
TINGS_DEFAULT_TING_FINDERS_FOLDER = os.path.join(
    TINGS_MODULE_BASE_FOLDER, "resources", "ting-finders"
)

TINGS_JINJA_ENV = get_global_jinja_env(delimiter_profile="frkl", env_type="native")

TINGS_INPUT_KEY = "_parent_tings_"

# ======================================
# Ting metadata defaults

NO_CACHE_KEY = "no_cache"
"""Key name for a value that states whether a property should be cached or not."""

# ======================================
# PluginTingFinder defaults

SEED_PATH_KEY = "_seed_path"
"""Path key name for the `FileTingFinderPlugin` plugin."""
REL_SEED_PATH_KEY = "_rel_seed_path"
"""Relative path key name for the `FileTingFinderPlugin` plugin."""

# DEFAULT_GENERIC_TING_FINDER_FIND_DATA = {
#     "plugin": "files",
#     "find_data": {
#         "base_paths": "{{:: base_paths ::}}",
#         "matchers": [{"path-regex": {"regex": "\\.tings$"}}],
#     },
#     "seed_spec": {
#         "properties": [
#             "id",
#             {
#                 "_type": "file-size",
#                 "path_property": SEED_PATH_KEY,
#                 "target_property": "_file_size",
#             },
#             {
#                 "_type": "file-name",
#                 "path_property": SEED_PATH_KEY,
#                 "target_property": "_file_name",
#             },
#         ]
#     },
# }
# """Default tings configuration when user does not specify their own."""
