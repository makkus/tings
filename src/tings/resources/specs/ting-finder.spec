ting_class_name: TingFinderTing
properties:
  - mirror:
      source_property: _id
      target_property: alias
  - mirror:
      source_property: _dict_content
      target_property: dict_content
  - dict-value:
      source_property: dict_content
      key: args
  - dict-value:
      source_property: dict_content
      key: finder
      target_property: finder_conf
  - ting-finder-obj:
      source_property: finder_conf
      finder_obj_property: ting_finder
      find_data_property: find_data
  - template-input-schema:
      source_property: finder_conf
      target_property: input_schema
      jinja_env_delimiter_profile: "frkl"
      jinja_env_type: "native"

addons:
  - create-find-data
  - create-tings
  - watch-tings
